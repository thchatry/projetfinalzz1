#include <SDL2/SDL.h>

int LARGEUR = 1400;
int HAUTEUR = 700;
int LIGNE = 28;
int COLONNE = 28;

int nbvoisin(int matrice[COLONNE][LIGNE], int col, int ligne) {
    // Sommme des 8 cases entourant (col,ligne)
    int nb = 
        matrice[(col-1 + COLONNE)%COLONNE][(ligne-1 + LIGNE)%LIGNE] +
        matrice[(col-1 + COLONNE)%COLONNE][ligne] + 
        matrice[(col-1 + COLONNE)%COLONNE][(ligne+1)%LIGNE] +
        matrice[col][(ligne-1+ LIGNE)%LIGNE] +
        matrice[col][(ligne+1)%LIGNE] + 
        matrice[(col+1)%COLONNE][(ligne-1+ LIGNE)%LIGNE] +
        matrice[(col+1)%COLONNE][ligne] +
        matrice[(col+1)%COLONNE][(ligne+1)%LIGNE];

    return nb;
}

void dessinMat(int mat[COLONNE][LIGNE], SDL_Renderer * renderer)
{
  int blanc = 255, noir = 0;
  SDL_Rect rectangle = {0,0,LARGEUR/COLONNE,HAUTEUR/LIGNE};
  for(int i = 0; i<COLONNE; i++)
  {
    for(int j = 0; j<LIGNE; j++)
    {
      rectangle.x = i*LARGEUR/COLONNE;
      rectangle.y = j*HAUTEUR/LIGNE;
      if(mat[i][j] == 0)
      {
        SDL_SetRenderDrawColor(renderer, blanc, blanc, blanc, blanc);
        SDL_RenderFillRect(renderer, &rectangle);
      }
      else
      {
        SDL_SetRenderDrawColor(renderer, noir, noir, noir, blanc);
        SDL_RenderFillRect(renderer, &rectangle);
      }
    }
  }
}

void jeudelavie(SDL_Renderer * renderer)
{
  int mat[COLONNE][LIGNE];
  int matvoisin[COLONNE][LIGNE];

  for(int i = 0; i<COLONNE; i++)
  {
    for(int j = 0; j<LIGNE; j++)
    {
      mat[i][j]=0;
    }
  }

  mat[5][5] = 1;
  mat[5][6] = 1;
  mat[4][6] = 1;
  mat[3][6] = 1;
  mat[4][4] = 1;

  mat[10][10] = 1;
  mat[10][11] = 1;
  mat[10][12] = 1;

  dessinMat(mat, renderer);
  SDL_RenderPresent(renderer);
  SDL_Delay(230);

  for(int p = 0; p<100; p++)
  {

    for(int i = 0; i<COLONNE; i++){
      for(int j = 0; j<LIGNE; j++){
        matvoisin[i][j] = nbvoisin(mat, i, j);
      }
    }

    for(int i = 0; i < COLONNE; i++)
    {
      for (int j = 0; j < LIGNE; j++)
      {
        if(mat[i][j] == 0)
        {
          if(matvoisin[i][j] == 3)
          {
            mat[i][j] = 1;
          }
        } 
        else
        {
          if(matvoisin[i][j] == 2 || matvoisin[i][j] == 3)
          {
            mat[i][j] = 1;
          }
          else
          {
            mat[i][j] = 0;
          }
        }
      }
    }

    dessinMat(mat, renderer);
    SDL_RenderPresent(renderer);
    SDL_Delay(150);

  }
}


int main(int argc, char **argv) {
  (void)argc;
  (void)argv;

  SDL_Window * fenetre = NULL;
  SDL_Renderer * renderer = NULL;

  fenetre = SDL_CreateWindow("Jeu de la Vie", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, LARGEUR, HAUTEUR, SDL_WINDOW_RESIZABLE);
  renderer = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED);

  jeudelavie(renderer);

  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(fenetre);

  SDL_Quit();
}