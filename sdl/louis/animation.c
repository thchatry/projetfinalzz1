#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

void end_sdl(char ok, char const * msg, SDL_Window* window, SDL_Renderer* renderer){
  char msg_formated[255];                                         
  int l;                                                          

  if (!ok){                                                      
         strncpy(msg_formated, msg, 250);                                 
         l = strlen(msg_formated);                                        
         strcpy(msg_formated + l, " : %s\n");                     

         SDL_Log(msg_formated, SDL_GetError());                   
  }                                                               

  if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
  if (window != NULL)   SDL_DestroyWindow(window);                                        

  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
}                            

SDL_Texture * load_texture_from_image(char * file_image_name, SDL_Window * window, SDL_Renderer * renderer){
    SDL_Surface *my_image = NULL;
    SDL_Texture* my_texture = NULL;

    my_image = IMG_Load(file_image_name);

    if (my_image == NULL) end_sdl(0, "Chargement de l'image impossible", window, renderer);

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
    SDL_FreeSurface(my_image);
    if (my_texture == NULL) end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

    return my_texture;
}

void animation1(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture * texture, SDL_Texture * texture2)
{
    SDL_Rect source = {0}, dimension = {0}, destination = {0}, source2 = {0}, destination2 = {0};

    SDL_GetWindowSize(fenetre, &dimension.w, &dimension.h);
    SDL_QueryTexture(texture, NULL, NULL, &source.w, &source.h);
    SDL_QueryTexture(texture2, NULL, NULL, &source2.w, &source2.h);

    destination2.w = 400;
    destination2.h = 600;
    destination2.x = (dimension.w - destination2.w) / 2;
    destination2.y = dimension.h-destination2.h;

    destination.w = 300;
    destination.h = 300;
    destination.x = (dimension.w - destination.w) / 2;
    destination.y = dimension.h - destination2.h - destination.h + 20;

    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, texture, &source, &destination);
    SDL_RenderCopy(renderer, texture2, &source2, &destination2);
    SDL_RenderPresent(renderer);
    SDL_Delay(2000);

    for(int i = 0; i<50; i++)
    {
        destination.y = destination.y + 5*i;
        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, texture, &source, &destination);
        SDL_RenderCopy(renderer, texture2, &source2, &destination2);
        SDL_RenderPresent(renderer);
        SDL_Delay(100);
    }
}

void animation2(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture * texture)
{
    SDL_Rect source = {0}, dimension = {0}, destination = {0}, state = {0};

    SDL_GetWindowSize(fenetre, &dimension.w, &dimension.h);
    SDL_QueryTexture(texture, NULL, NULL, &source.w, &source.h);

    //int nbimg = 40;
    int offsetx = source.w / 10;
    int offsety = source.h / 4;

    state.x = 0;
    state.y = 0;
    state.w = offsetx;
    state.h = offsety;

    destination.w = 200;
    destination.h = 200;
    destination.x = (dimension.w - destination.w)/2;
    destination.y = (dimension.h - destination.h)/2;

    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, texture, &state, &destination);
    SDL_RenderPresent(renderer);
    SDL_Delay(80);

    for(int i = 0; i<100; i++)
    {
        state.x = (state.x + offsetx);
        state.x %= source.w;
        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, texture, &state, &destination);
        SDL_RenderPresent(renderer);
        SDL_Delay(80);

    }

}

int main(int argc, char ** argv)
{
    (void)argc;
    (void)argv;

    int LARGEUR = 1000;

    SDL_Window * fenetre = NULL;
    SDL_Renderer * renderer = NULL;
    SDL_Texture * texture = NULL;
    SDL_Texture * texture2 = NULL;
    SDL_Texture * texture3 = NULL;

    fenetre = SDL_CreateWindow("Animation", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, LARGEUR, LARGEUR, SDL_WINDOW_RESIZABLE);
    renderer = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED);
    texture = load_texture_from_image("./img/mario.png", fenetre, renderer);
    texture2 = load_texture_from_image("./img/tuyau.png", fenetre, renderer);
    texture3 = load_texture_from_image("./img/link2.png", fenetre, renderer);

    animation1(fenetre, renderer, texture, texture2);
    animation2(fenetre, renderer, texture3);

    SDL_DestroyTexture(texture);
    SDL_DestroyTexture(texture2);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(fenetre);

    SDL_Quit();
}