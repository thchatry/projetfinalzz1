#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

int LARGEUR = 1400;
int HAUTEUR = 700;
int LIGNE = 28;
int COLONNE = 28;

void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer){
  char msg_formated[255];                                         
  int l;                                                          

  if(!ok)
  {                                                      
    strncpy(msg_formated, msg, 250);                                 
    l = strlen(msg_formated);                                        
    strcpy(msg_formated + l, " : %s\n");                     
    SDL_Log(msg_formated, SDL_GetError());                   
  }                                                               

  if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
  if (window != NULL)   SDL_DestroyWindow(window);                                        

  SDL_Quit();                                                     

  if(!ok)
  {                                                      
    exit(EXIT_FAILURE);                                              
  }                                                               
}  

int nbvoisin(int matrice[COLONNE][LIGNE], int col, int ligne) {
    int nb = 
        matrice[(col-1 + COLONNE)%COLONNE][(ligne-1 + LIGNE)%LIGNE] +
        matrice[(col-1 + COLONNE)%COLONNE][ligne] + 
        matrice[(col-1 + COLONNE)%COLONNE][(ligne+1)%LIGNE] +
        matrice[col][(ligne-1+ LIGNE)%LIGNE] +
        matrice[col][(ligne+1)%LIGNE] + 
        matrice[(col+1)%COLONNE][(ligne-1+ LIGNE)%LIGNE] +
        matrice[(col+1)%COLONNE][ligne] +
        matrice[(col+1)%COLONNE][(ligne+1)%LIGNE];

    return nb;
}

void dessinMat(int mat[COLONNE][LIGNE], SDL_Renderer * renderer)
{
  int blanc = 255, noir = 0;
  SDL_Rect rectangle = {0,0,LARGEUR/COLONNE,HAUTEUR/LIGNE};
  for(int i = 0; i<COLONNE; i++)
  {
    for(int j = 0; j<LIGNE; j++)
    {
      rectangle.x = i*LARGEUR/COLONNE;
      rectangle.y = j*HAUTEUR/LIGNE;
      if(mat[i][j] == 0)
      {
        SDL_SetRenderDrawColor(renderer, blanc, blanc, blanc, blanc);
        SDL_RenderFillRect(renderer, &rectangle);
      }
      else
      {
        SDL_SetRenderDrawColor(renderer, noir, noir, noir, blanc);
        SDL_RenderFillRect(renderer, &rectangle);
      }
    }
  }
}

void jeudelavie(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture * texture)
{
  int mat[COLONNE][LIGNE];
  int matvoisin[COLONNE][LIGNE];
  int temps = 100, posx, posy;

  SDL_Rect pos = {0, 0, 0, 0};
  SDL_QueryTexture(texture, NULL, NULL, &pos.w, &pos.h);
  SDL_GetWindowSize(fenetre, &pos.x, &pos.y);
  pos.x = pos.x/4-75;
  pos.y = pos.y/4;

  SDL_bool prog_on = SDL_TRUE, vivant = SDL_TRUE, ajout = SDL_FALSE,
    start = SDL_FALSE;
  SDL_Event event;

  for(int i = 0; i<COLONNE; i++)
  {
    for(int j = 0; j<LIGNE; j++)
    {
      mat[i][j]=0;
    }
  }

  mat[5][5] = 1;
  mat[5][6] = 1;
  mat[4][6] = 1;
  mat[3][6] = 1;
  mat[4][4] = 1;

  mat[10][10] = 1;
  mat[10][11] = 1;
  mat[10][12] = 1;

  while(prog_on)
  {

    while(prog_on && SDL_PollEvent(&event))
    {
      switch(event.type)
      {
        case SDL_QUIT:
          prog_on = SDL_FALSE;
          break;

        case SDL_KEYDOWN:
          switch (event.key.keysym.sym) {
            case SDLK_SPACE:   
             if(start) vivant = !vivant;
            break;
            case SDLK_LEFT:
              temps += 10;
              break;
            case SDLK_RIGHT:
              if(temps>19) temps = temps - 10;
              break;
            case SDLK_ESCAPE:
              prog_on = SDL_FALSE;
              break;
            case SDLK_s:
              start = !start;
            default:
              break;
          }
          break;

        case SDL_MOUSEBUTTONDOWN:
          if(SDL_GetMouseState(&posx, &posy) && SDL_BUTTON(SDL_BUTTON_LEFT))
          {
            ajout = SDL_TRUE;
          }
          break;

        default:
          break;
      }
    }

    if(!start)
    {
      dessinMat(mat, renderer);
      SDL_RenderCopy(renderer, texture, NULL, &pos);
      SDL_RenderPresent(renderer);
      SDL_Delay(temps);
    }

    if(vivant && start)
    {
      for(int i = 0; i<COLONNE; i++)
      {
        for(int j = 0; j<LIGNE; j++)
        {
          matvoisin[i][j] = nbvoisin(mat, i, j);
        }
      }

      for(int i = 0; i < COLONNE; i++)
      { 
        for (int j = 0; j < LIGNE; j++)
        {
          if(mat[i][j] == 0)
          {
            if(matvoisin[i][j] == 3)
            { 
              mat[i][j] = 1;
            }
          } 
          else
          {
            if(matvoisin[i][j] == 2 || matvoisin[i][j] == 3)
            {
              mat[i][j] = 1;
            }
            else
            {
              mat[i][j] = 0;
            }
          }
        }
      }
      dessinMat(mat, renderer);
      SDL_RenderPresent(renderer);
      SDL_Delay(temps);
    }
    else if(ajout && start)
    {
      posx = posx*((float)COLONNE/LARGEUR);
      posy = posy*((float)LIGNE/HAUTEUR);
      mat[posx][posy] = !mat[posx][posy]; 
      dessinMat(mat, renderer); 
      SDL_RenderPresent(renderer);
      ajout = SDL_FALSE;

    }
  }
}


int main(int argc, char **argv) {
  (void)argc;
  (void)argv;

  SDL_Window * fenetre = NULL;
  SDL_Renderer * renderer = NULL;

  fenetre = SDL_CreateWindow("Jeu de la Vie", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, LARGEUR, HAUTEUR, SDL_WINDOW_RESIZABLE);
  if (fenetre == NULL) end_sdl(0, "ERROR WINDOW CREATION", fenetre, renderer);

  renderer = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED);
  if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", fenetre, renderer);

  if (TTF_Init() < 0) end_sdl(0, "Couldn't initialize SDL TTF", fenetre, renderer);
  
  TTF_Font * font = NULL;
  font = TTF_OpenFont("./fonts/sweet.ttf", 150);
  if (font == NULL) end_sdl(0, "Can't load font", fenetre, renderer);

  SDL_Color color = {130, 130, 130, 255};
  SDL_Surface * surface = NULL;
  surface = TTF_RenderText_Blended(font, "Jeu de la Vie", color);
  if (surface == NULL) end_sdl(0, "Can't create text surface", fenetre, renderer);
  TTF_CloseFont(font);

  SDL_Texture * texte = NULL;
  texte = SDL_CreateTextureFromSurface(renderer, surface);
  if (texte == NULL) end_sdl(0, "Can't create texture from surface", fenetre, renderer);
  SDL_FreeSurface(surface);
  

  jeudelavie(fenetre, renderer, texte);

  end_sdl(1, "Normal ending", fenetre, renderer);

  SDL_DestroyTexture(texte);

  TTF_Quit();
}