#include <SDL2/SDL.h>
#include <stdio.h>

int main(int argc, char **argv) {
  (void)argc;
  (void)argv;

  SDL_Window * tab[40];

  /* Initialisation de la SDL  + gestion de l'échec possible */
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());      // l'initialisation de la SDL a échoué 
    exit(EXIT_FAILURE);
  }

  int largeur = 0, hauteur = 360, i = 0;

  for(i=0; i<40; i++){
      tab[i] = SDL_CreateWindow(
          "Fenêtre",
          largeur, hauteur,
          300, 300,
          SDL_WINDOW_RESIZABLE);
      
      largeur+=50;
  }
  
  int posx, posy;

  /* Normalement, on devrait ici remplir les fenêtres... */
  SDL_Delay(2000);                           // Pause exprimée  en ms

  for(int j = 0; j<20; j++){
    for(i = 0; i<20; i++){
      SDL_GetWindowPosition(tab[2*i], &posx, &posy);
      SDL_SetWindowPosition(tab[2*i], posx, posy-20);
      SDL_GetWindowPosition(tab[2*i+1], &posx, &posy);
      SDL_SetWindowPosition(tab[2*i+1], posx, posy+20);
      SDL_Delay(42);
    }
  }

  /* et on referme tout ce qu'on a ouvert en ordre inverse de la création */
  for(i = 39; i>-1; i--){
      SDL_DestroyWindow(tab[i]);
  }

  SDL_Quit();
  return 0;
}