#include <SDL2/SDL.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char **argv) {
  (void)argc;
  (void)argv;

  SDL_Window * fenetre = NULL;
  SDL_Renderer * renderer = NULL;
  SDL_Event event;
  SDL_bool prog_on = SDL_TRUE;

  int largeur = 720, hauteur = 480;

  SDL_Rect tab[10];
  SDL_Rect tab1[13];
  SDL_Rect tab2[7];

  for(int i = 0; i<10; i++){
    tab[i].x = largeur/8;
    tab[i].y = hauteur*i/20;
    tab[i].w = largeur/20;
    tab[i].h = hauteur/20;
  }

  for(int i = 0; i<13; i++)
  {
    tab1[i].x = largeur - largeur*i/20;;
    tab1[i].y = hauteur/3;
    tab1[i].w = largeur/20;
    tab1[i].h = hauteur/20;
  }

  for(int i = 0; i<7; i++)
  {
    tab2[i].x = largeur*i/20;;
    tab2[i].y = hauteur/6 + hauteur*i/20;
    tab2[i].w = largeur/20;
    tab2[i].h = hauteur/20;
  }

  if(SDL_VideoInit(NULL) < 0) // Initialisation de la SDL
    {
        printf("Erreur d'initialisation de la SDL : %s",SDL_GetError());
        return EXIT_FAILURE;
    }


  fenetre = SDL_CreateWindow("Le Serpent", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, largeur, hauteur, SDL_WINDOW_RESIZABLE);
  renderer = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED);

  SDL_SetRenderDrawColor(renderer, 0, 100, 0, 255); // Fond vert.
  SDL_RenderClear(renderer);

  SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
  for(int i = 0; i<10; i++){
      SDL_RenderDrawRects(renderer, tab, i);
  }

  SDL_SetRenderDrawColor(renderer, 123, 0, 150, 255);
  for(int i = 0; i<13; i++){
      SDL_RenderDrawRects(renderer, tab1, i);
  }

  SDL_SetRenderDrawColor(renderer, 123, 200, 150, 255);
  for(int i = 0; i<7; i++){
      SDL_RenderDrawRects(renderer, tab2, i);
  }

  SDL_RenderPresent(renderer);
  SDL_Delay(100);

  while(prog_on)
  {
    while(prog_on && SDL_PollEvent(&event))
    {
      switch(event.type)
      {
        case SDL_QUIT:
          prog_on = SDL_FALSE;
          break;
        
        default:
          break;
      }
    }

    for(int j = 0; j<10; j++){
      tab[j].y = (tab[j].y + hauteur/20)%hauteur;
    }

    for(int i = 0; i<13; i++){
      tab1[i].x = (tab1[i].x + largeur/20)%largeur;
    }

    for(int i = 0; i<7; i++){
      tab2[i].x = (tab2[i].x + largeur/20)%largeur;
      tab2[i].y = (tab2[i].y + hauteur/20)%hauteur;
    }

    SDL_SetRenderDrawColor(renderer, 0, 100, 0, 255);
    SDL_RenderClear(renderer);

    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderDrawRects(renderer, tab, 10);

    SDL_SetRenderDrawColor(renderer, 123, 0, 150, 255);
    SDL_RenderDrawRects(renderer, tab1, 13);

    SDL_SetRenderDrawColor(renderer, 123, 200, 150, 255);
    SDL_RenderDrawRects(renderer, tab2, 7);

    SDL_RenderPresent(renderer);
  SDL_Delay(100);
  }

  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(fenetre);

  SDL_Quit();

  return 0;
}
