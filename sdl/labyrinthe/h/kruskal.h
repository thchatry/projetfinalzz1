#ifndef KRUSKAL_H
#define KRUSKAL_H

#include "graph.h"
#include "partition_arbo.h"

#include <stdlib.h>
#include <stdio.h>
#include <time.h>



void echangerArc(Arc * a, Arc * b);
void tri_arrete(Graph * graph);
void fisher_yate(Graph * graphe);
Graph * kruskal(Graph * graphe);
Graph * kruskalDensiteP(Graph * graph, float p);

#endif