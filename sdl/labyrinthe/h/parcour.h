#ifndef PARCOUR_H
#define PARCOUR_H


#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>

#include "graph.h"
#include "matrice.h"
#include "pile.h"
#include "dessin_lab.h"
#include "kruskal.h"
#include "dijkstra.h"

void parcoursProfondeur(Graph * graphe, int n, int p, int depart);
int * coordonnees(int ind, int p);
void explorer(Pile ** pile, int ** matrice, int ** explore, int sommet, int p, int * coord);
void controlePetitBonhomme(SDL_Window * fenetre, Graph * graphe, int n, int p);
void monstre(SDL_Window * fenetre, Graph * graphe, int n, int p);
void lampeTorche(int ** matriceFlags, int ** matriceNoeudExplore, int * coord, int dir);
void jeu();
void aleaMonstre(int ** matrice, int ** coordMonstre, int * sommetMonstre, int * dirMonstre, int nbMonstre, int p);

#endif