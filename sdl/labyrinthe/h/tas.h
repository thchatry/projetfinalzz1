#ifndef TAS_H
#define  TAS_H


#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "graph.h"


typedef struct
{
    int taille;
    int capacite;
    int * liste;
} Tas;

Tas * init_tas(int t);
void liberer_Tas(Tas * tas);
void echanger(int * a, int * b);
void tamiser(Tas * tas, int noeud, int n, int* distance);
void construction(Tas * tas, int * contig, int tai);
void tri_par_tas(Tas * tas, int* distance);
void afficherTas(Tas * tas);
int comparator(const void *p, const void *q);
int liberer(Tas * tas);
void ajouter(Tas * tas, int nouv);

#endif