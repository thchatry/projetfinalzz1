#ifndef _PILE_H_
#define _PILE_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct Pile
{
    int element;
    struct Pile * suivant;
}Pile;

Pile ** creaPile();

/*  Libère l'emplacement mémoire alloué pour la pile                                */

void libererPile(Pile ** pile);

/*  Ajout d'un élément sur le haut de la pile/au début de la liste chainée      */

void empiler(Pile ** pile, int empilade);

/*  Suppression de la valeur en haut de la pile                                                           */

void depiler(Pile ** pile);

/*  Retourne le sommet de la pile (le premier élément de la liste chainée)      */

int sommet(Pile ** pile);

/*  Vérifie que la pile est vide                                                                */

int pileEstVide(Pile ** pile);

#endif