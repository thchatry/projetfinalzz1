#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include <math.h>

#include "tas.h"
#include "graph.h"



int* dijkstra(Graph* graph, int depart);
int* dijkstra2(Graph* graph, int depart);
void initialisation(Graph* graph, int depart, int* distance, int* etat, int* predecesseur);

void maj_distances(int s1, int s2, int poid, int* d, int* predecesseur);
void maj_distancesEucli(int s1, int s2, int fin, int poid, int* d, int* predecesseur, int p);
void maj_distancesTchey(int s1, int s2, int fin, int poid, int* d, int* predecesseur, int p);
void maj_distancesManhattan(int s1, int s2, int fin, int poid, int* d, int* predecesseur, int p);

void ajoutEnTete(Voisin ** tete, Voisin * nouv);
Voisin * creerVoisin(int sommet, int poid);
int* aEtoile(Graph* graph, int depart, int arrivee, int p, int mode);
void libererVoisins(Voisin ** voisins, int nb);
int max(int a, int b);
int square(int a);

int distanceEuclidienne(int courant, int fin, int p);
int distTchey(int courant, int fin, int p);
int distManhattan(int courant,int fin, int p);



#endif