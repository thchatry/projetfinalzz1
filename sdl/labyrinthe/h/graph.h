#ifndef GRAPH_H
#define GRAPH_H


#include <stdlib.h>
#include <stdio.h>
#include <time.h>

typedef struct Arc {
    int sommet_1;
    int sommet_2;
    int poid;
} Arc;

typedef struct Graph{
    int nbNoeuds;
    int nbArcs;
    Arc** arcs;
} Graph;

typedef struct Voisin{
    int sommet;
    int poid;
    struct Voisin * suiv;
} Voisin;

Graph * creerGraphe(int n, int nb);
Graph * creerGrapheGrille(int n, int p);
Arc* creerArc(int a, int b, int poid);
void afficheGraph(Graph* graph);
void ajoutArc(Graph* graph, int sommet_1, int sommet_2, int poid);
void libererGraph(Graph * graph);
void afficherListeArrete(Graph* graph);
void afficheTab(int* tab, int n);


#endif