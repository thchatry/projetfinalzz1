#ifndef DESSIN_LAB_H
#define DESSIN_LAB_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "parcour.h"

void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer);
void dessin_lab(int ** matrice, int a, int b);
SDL_Texture * load_texture_from_image(char * file_image_name, SDL_Window * window, SDL_Renderer * renderer);
SDL_Texture * load_texture_from_font(char * file_font_name, int size_font, SDL_Color color, char * text_to_show, SDL_Window * window, SDL_Renderer * renderer);
void dessinLabTilesetExplore(SDL_Window * fenetre, SDL_Renderer* renderer, SDL_Texture * texture, int ** matrice, int ** explore, int a, int b);
void dessinBonhomme(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture * texture, int * coord, int n, int p);
void dessinBonhommeQuiTourne(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture * texture, int * coord, int n, int p, int dir);
void dessinBonhommeAnimation(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture * textureMur,
                                SDL_Texture * textureBonhomme, SDL_Texture * textureFin, SDL_Texture ** textureTitre, int ** matrice,
                                int ** explore, int * coord, int * finish, int n, int p, int dir);

void dessinAnimation(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture * textureMur,
                    SDL_Texture * textureBonhomme, SDL_Texture * textureMonstre, SDL_Texture * textureSmart, SDL_Texture * textureFin, 
                    int ** matrice, int ** coordMonstre, int * coordSmart, int * coord, int * finish,
                    int * dirMonstre, int dirSmart, int n, int p, int dir, int nbMonstre, int ** signe);

void dessinEcranTitre(SDL_Window * fenetre, SDL_bool * noir, SDL_bool * survival, Graph * graphe, int n, int p);
void titre(SDL_Rect * dimension, SDL_Rect * source, SDL_Rect * destination);
void dark(SDL_Rect * dimension, SDL_Rect * source, SDL_Rect * destination);
void survie(SDL_Rect * dimension, SDL_Rect * source, SDL_Rect * destination);

#endif