#ifndef PARTITION_ARBO_H
#define PARTITION_ARBO_H

#include <stdio.h>
#include <stdlib.h>

int * tableau_hauteur(int valeur);
int * creer(int valeur);
int recuperer_classe(int a, int * partition);
void fusion(int a, int b, int * partition, int * hauteur);
void lister_classe(int a, int * partition, int valeur);
void lister(int * partition, int valeur, int * hauteur);
void afficher_tab(int * partition, int valeur);


#endif