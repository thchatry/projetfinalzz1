#ifndef MATRICE_H
#define MATRICE_H

#include "graph.h"
#include <stdlib.h>
#include <stdio.h>

int ** graphe_lab_vers_matrice(Graph * labyrinthe, int a, int b);
void liberer_matrice(int ** matrice, int n);
void afficheMatrice(int n, int m, int** matrice);
void initMatriceValeur(int ** matriceFlags, int n, int p, int valeur);

#endif