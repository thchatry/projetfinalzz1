#ifndef GRAPHVIZ_H
#define GRAPHVIZ_H

#include "graph.h"

#include <graphviz/gvc.h>
#include <graphviz/cgraph.h>

int crea_graphviz_partition(int * partition, int n, char * nom_png);
int crea_graphviz_graphe(Graph * graphe, char * nom_png);

#endif