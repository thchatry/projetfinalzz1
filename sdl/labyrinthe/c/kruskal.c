#include "../h/kruskal.h"


void echangerArc(Arc * a, Arc * b)
{
    Arc* c = NULL;
    c = malloc(sizeof(Arc));
    if(c == NULL) exit(1);

    c->sommet_1 = a->sommet_1;
    c->sommet_2 = a->sommet_2;
    c->poid = a->poid;
    
    a->sommet_1 = b->sommet_1;
    a->sommet_2 = b->sommet_2;
    a->poid = b->poid;

    b->sommet_1 = c->sommet_1;
    b->sommet_2 = c->sommet_2;
    b->poid = c->poid;
}

void tri_arrete(Graph * graph)
{
    int min = 100;
    int ind = 0;

    for(int i = 0; i<graph->nbNoeuds; i++)
    {
        for(int j = i; j<graph->nbNoeuds; j++)
        {
            if(graph->arcs[j]->poid < min)
            {
                min = graph->arcs[j]->poid;
                ind = j;
            }
        }


        echangerArc(graph->arcs[i], graph->arcs[ind]);

        min = 100;
        ind = i+1;
    }
}

void fisher_yate(Graph * graph)
{
    srand(time(NULL));
    int random;
    for(int i = (graph->nbArcs)-1; i>-1; i--)
    {
        random = rand()%(i+1);
        echangerArc(graph->arcs[i], graph->arcs[random]);
    }
}

Graph * kruskal(Graph * graph)
{
    int * partition = creer(graph->nbNoeuds);
    int * hauteur = tableau_hauteur(graph->nbNoeuds);

    Graph * minimal = malloc(sizeof(Graph));
    minimal->nbNoeuds = graph->nbNoeuds;
    minimal->arcs = malloc(graph->nbArcs * sizeof(Arc *));

    for(int i = 0; i<graph->nbArcs; i++)
    {
        if(recuperer_classe(graph->arcs[i]->sommet_1, partition) != recuperer_classe(graph->arcs[i]->sommet_2, partition))
        {
            fusion(graph->arcs[i]->sommet_1, graph->arcs[i]->sommet_2, partition, hauteur);
            ajoutArc(minimal, graph->arcs[i]->sommet_1, graph->arcs[i]->sommet_2, graph->arcs[i]->poid);
        }
    }

    return minimal;
}

Graph * kruskalDensiteP(Graph * graph, float p)
{
    srand(time(NULL));
    float alpha = 0;
    int * partition = creer(graph->nbNoeuds);
    int * hauteur = tableau_hauteur(graph->nbNoeuds);

    Graph * minimal = malloc(sizeof(Graph));
    minimal->nbNoeuds = graph->nbNoeuds;
    minimal->arcs = malloc(graph->nbArcs * sizeof(Arc *));

    for(int i = 0; i<graph->nbArcs; i++)
    {
        alpha = (float)(rand()%101) / (float)100;
        if(recuperer_classe(graph->arcs[i]->sommet_1, partition) != recuperer_classe(graph->arcs[i]->sommet_2, partition) || alpha < p)
        {
            fusion(graph->arcs[i]->sommet_1, graph->arcs[i]->sommet_2, partition, hauteur);
            ajoutArc(minimal, graph->arcs[i]->sommet_1, graph->arcs[i]->sommet_2, graph->arcs[i]->poid);
        }
    }

    return minimal;
}