#include "../h/partition_arbo.h"

int * tableau_hauteur(int valeur)
{
    int * hauteur = malloc(valeur * sizeof(int));
    for(int i = 0; i<valeur; i++)
    {
        hauteur[i] = 1;
    }
    return hauteur;
}

int * creer(int valeur)
{
    int * partition = malloc(valeur * sizeof(int));
    for(int i = 0; i<valeur; i++)
    {
        partition[i] = i;
    }
    return partition;
}

int recuperer_classe(int a, int * partition)
{
    int res = a;
    while(partition[res] != res)
    {
        res = partition[res];
    }
    return res;
}

void fusion(int a, int b, int * partition, int * hauteur)
{
    int res1 = recuperer_classe(a, partition);
    int res2 = recuperer_classe(b, partition);
    if(hauteur[res1] > hauteur[res2])
    {
        partition[res2] = res1;
        hauteur[res2] = 0;
    }
    else if(hauteur[res2] > hauteur[res1])
    {
        partition[res1] = res2;
        hauteur[res1] = 0;
    }
    else
    {
        partition[res2] = res1;
        hauteur[res1]++;
        hauteur[res2] = 0;
    }
}

void lister_classe(int a, int * partition, int valeur)
{
    printf("{ ");
    for(int i = 0; i<valeur; i++)
    {
        if(recuperer_classe(i, partition) == a) printf("%d ", i);
    }
    printf("}");

}

void lister(int * partition, int valeur, int * hauteur)
{
    printf("{");
    for(int i = 0; i<valeur; i++)
    {   
        if(hauteur[i] != 0)
        {
            lister_classe(i, partition, valeur);
        }
    }
    printf("}\n\n");
}

void afficher_tab(int * partition, int valeur)
{
    for(int i = 0; i<valeur; i++)
    {
        printf("%d ", i);
    }
    printf("\n");
    for(int i = 0; i<valeur; i++)
    {
        printf("%d ", partition[i]);
    }
    printf("\n");
}
/*
int main()
{

    int N = 21;
    int NB = 50;

    int * partition = creer(N);
    int * hauteur = tableau_hauteur(N);

    graphe_t * graphe = crea_graphe(N, NB);
    tri_arrete(graphe, NB);
    graphe_t * minimal = kruskal(graphe, partition, hauteur, N, NB);
    afficher_liste_arrete(graphe, NB);
    afficher_liste_arrete(minimal, NB);
    crea_graphviz_graphe(graphe, NB, "graphe1.png");
    crea_graphviz_graphe(minimal, NB, "graphe2.png");

    liberer_graphe(graphe, NB);
    liberer_graphe(minimal, NB);
    free(partition);
    free(hauteur);
    

    int a = 20;
    int b = 20;
    int c = a*(b-1)+b*(a-1);

    int * partition_lab = creer(a*b);
    int * hauteur_lab = tableau_hauteur(a*b);

    graphe_t * grille = crea_graphe_grille(a, b);
    fisher_yate(grille, c);
    graphe_t * labyrinthe = kruskal(grille, partition_lab, hauteur_lab, a*b, c);
    //crea_graphviz_graphe(labyrinthe, c, "labyrinthe.png");

    afficher_liste_arrete(grille, c);
    afficher_liste_arrete(labyrinthe, c);

    int ** matrice = graphe_lab_vers_matrice(labyrinthe, a, b);
    //int ** matrice = matrice_adja(a);

    afficher_matrice(matrice, a, b);

    dessin_lab(matrice, a, b);

    liberer_matrice(matrice, a);
    liberer_graphe(grille, c);
    liberer_graphe(labyrinthe, c);
    free(partition_lab);
    free(hauteur_lab);

    return 0;
}
*/