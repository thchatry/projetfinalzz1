#include "../h/dijkstra.h"

int* dijkstra(Graph* graph, int depart) {


    int* distance = NULL;
    int* predecesseur = NULL;
    int* etat = NULL;

    int s1, s2;
    Arc* arc;

    Tas* tas;
    tas = init_tas(1000000);

    distance = malloc((graph->nbNoeuds)*sizeof(int));
    if(distance == NULL) exit(1);

    predecesseur = malloc((graph->nbNoeuds)*sizeof(int));
    if(predecesseur == NULL) exit(1);

    etat = malloc((graph->nbNoeuds)*sizeof(int));
    if(etat == NULL) exit(1);

    initialisation(graph, depart, distance, etat, predecesseur);

    for(int i = 0; i < graph->nbNoeuds; i++) {
        ajouter(tas, i);
    }

    tri_par_tas(tas, distance);
    //afficherTas(tas);

    while(tas->taille > 0) {
        s1 = liberer(tas);
        etat[s1] = 1;
        for(int i = 0; i < graph->nbArcs; i++) {
            arc = graph->arcs[i];
            s2 = -1;
            if(arc->sommet_1 == s1) {
                s2 = arc->sommet_2;
            } else if (arc->sommet_2 == s1) {
                s2 = arc->sommet_1;
            }
            if(s2 != -1) {       
                maj_distances(s1, s2 , arc->poid, distance, predecesseur);
            }
        }
        tri_par_tas(tas, distance);
    }
    //afficheTab(predecesseur, graph->nbNoeuds);
    return predecesseur;
}

int* dijkstra2(Graph* graph, int depart) {


    int* distance = NULL;
    int* predecesseur = NULL;
    int* etat = NULL;
    Voisin** voisins = NULL;
    Voisin * cour;

    int s1, s2;
    int aTraite = graph->nbNoeuds;
    Arc* arc;

    Tas* tas;
    tas = init_tas(1000000);

    distance = malloc((graph->nbNoeuds)*sizeof(int));
    if(distance == NULL) exit(1);

    predecesseur = malloc((graph->nbNoeuds)*sizeof(int));
    if(predecesseur == NULL) exit(1);

    voisins = malloc((graph->nbNoeuds) * sizeof(Voisin*));
    if(voisins == NULL) exit(1);

    etat = malloc((graph->nbNoeuds)*sizeof(int));
    if(etat == NULL) exit(1);

    initialisation(graph, depart, distance, etat, predecesseur);


    // Initialisation des tableaux voisins et etat
    for(int i = 0; i < graph->nbNoeuds; i++) {
        voisins[i] = NULL;
        etat[i] = 0;
    }

    // Initialisation des voisins
    for(int  i = 0; i < graph->nbArcs; i++) {
        arc = graph->arcs[i];

        ajoutEnTete(&voisins[arc->sommet_1], creerVoisin(arc->sommet_2, arc->poid));
        ajoutEnTete(&voisins[arc->sommet_2], creerVoisin(arc->sommet_1, arc->poid));
        
    }

    ajouter(tas, depart);

    // Debut algo
    while(aTraite > 0) {
        s1 = liberer(tas);
        etat[s1] = 2;
        aTraite--;
        cour = voisins[s1];
        while(cour != NULL) {
            if(etat[cour->sommet] == 0) {
                etat[cour->sommet] = 1;
                ajouter(tas, cour->sommet);
            }
            maj_distances(s1, cour->sommet, cour->poid, distance, predecesseur);
            cour = cour->suiv;
        }
        tri_par_tas(tas, distance);
    }



    // Liberation
    liberer_Tas(tas);
    free(distance);
    free(etat);
    libererVoisins(voisins, graph->nbNoeuds);
    return predecesseur;
}

int* aEtoile(Graph* graph, int depart, int arrivee, int p, int mode) {


    int* distance = NULL;
    int* predecesseur = NULL;
    int* etat = NULL;
    Voisin** voisins = NULL;
    Voisin * cour;

    int s1 = depart;
    int s2;
    int aTraite = graph->nbNoeuds;
    int iteration = 0;
    Arc* arc;

    Tas* tas;
    tas = init_tas(1000000);

    distance = malloc((graph->nbNoeuds)*sizeof(int));
    if(distance == NULL) exit(1);

    predecesseur = malloc((graph->nbNoeuds)*sizeof(int));
    if(predecesseur == NULL) exit(1);

    voisins = malloc((graph->nbNoeuds) * sizeof(Voisin*));
    if(voisins == NULL) exit(1);

    etat = malloc((graph->nbNoeuds)*sizeof(int));
    if(etat == NULL) exit(1);

    initialisation(graph, depart, distance, etat, predecesseur);


    // Initialisation des tableaux voisins et etat
    for(int i = 0; i < graph->nbNoeuds; i++) {
        voisins[i] = NULL;
        etat[i] = 0;
    }

    // Initialisation des voisins
    for(int  i = 0; i < graph->nbArcs; i++) {
        arc = graph->arcs[i];

        ajoutEnTete(&voisins[arc->sommet_1], creerVoisin(arc->sommet_2, arc->poid));
        ajoutEnTete(&voisins[arc->sommet_2], creerVoisin(arc->sommet_1, arc->poid));
        
    }

    ajouter(tas, depart);
    switch(mode){
                case 1:
                    distance[depart] = distanceEuclidienne(depart, arrivee, p);
                    break;
                case 2:
                    distance[depart] = distTchey(depart, arrivee, p);
                    break;
                case 3:
                    distance[depart] = distManhattan(depart, arrivee, p);
                    break;
                default:
                    distance[depart] = distanceEuclidienne(depart, arrivee, p);
                    break;
            }

    // Debut algo
    while(aTraite > 0 && s1 != arrivee) {
        iteration++;
        s1 = liberer(tas);
        etat[s1] = 2;
        aTraite--;
        cour = voisins[s1];
        while(cour != NULL) {
            if(etat[cour->sommet] == 0) {
                etat[cour->sommet] = 1;
                ajouter(tas, cour->sommet);
            }

            switch(mode){
                case 1:
                    maj_distancesEucli(s1, cour->sommet, arrivee,  cour->poid, distance, predecesseur, p);
                    break;
                case 2:
                    maj_distancesTchey(s1, cour->sommet, arrivee,  cour->poid, distance, predecesseur, p);
                    break;
                case 3:
                    maj_distancesManhattan(s1, cour->sommet, arrivee,  cour->poid, distance, predecesseur, p);
                    break;
                default:
                    maj_distancesEucli(s1, cour->sommet, arrivee,  cour->poid, distance, predecesseur, p);
                    break;
            }
        
            cour = cour->suiv;
        }
        tri_par_tas(tas, distance);
    }


    //printf("%d iterations pour %d\n", iteration, mode);
    // Liberation
    liberer_Tas(tas);
    free(distance);
    free(etat);
    libererVoisins(voisins, graph->nbNoeuds);
    return predecesseur;
}

void initialisation(Graph* graph, int depart, int* distance, int* etat, int* predecesseur) {

    int max;

    for(int i = 0; i < graph->nbArcs; i++) {
        max += graph->arcs[i]->poid;
    }
    max++;

    for(int i = 0; i < graph->nbNoeuds; i++) {
        distance[i] = max;
        etat[i] = 0;
        predecesseur[i] = -1;
    }

    predecesseur[depart] = depart;
    distance[depart] = 0;
}

void maj_distancesEucli(int s1, int s2, int fin, int poid, int* d, int* predecesseur, int p) {

    int nouv = d[s1] - distanceEuclidienne(s1, fin, p) + poid + distanceEuclidienne(s2, fin, p); 

    if (d[s2] > nouv) {     
       d[s2] = nouv;
       predecesseur[s2] = s1;
    }
}

void maj_distancesTchey(int s1, int s2, int fin, int poid, int* d, int* predecesseur, int p) {

    int nouv = d[s1] - distTchey(s1, fin, p) + poid + distTchey(s2, fin, p); 

    if (d[s2] > nouv) {     
       d[s2] = nouv;
       predecesseur[s2] = s1;
    }
}

void maj_distancesManhattan(int s1, int s2, int fin, int poid, int* d, int* predecesseur, int p) {

    int nouv = d[s1] - distManhattan(s1, fin, p) + poid + distManhattan(s2, fin, p); 

    if (d[s2] > nouv) {     
       d[s2] = nouv;
       predecesseur[s2] = s1;
    }
}

void maj_distances(int s1, int s2, int poid, int* d, int* predecesseur) {

    if (d[s2] > d[s1] + poid) {     
       d[s2] = d[s1] + poid;
       predecesseur[s2] = s1;
    }
}

Voisin * creerVoisin(int sommet, int poid) {
    Voisin * voisin = NULL;
    voisin = malloc(sizeof(Voisin));
    if(voisin == NULL) exit(1);

    voisin->sommet = sommet;
    voisin->poid = poid;

    return voisin;
}

void ajoutEnTete(Voisin ** tete, Voisin * nouv) {

    nouv->suiv = (*tete);
    (*tete) = nouv;
}

void libererVoisins(Voisin ** voisins, int nb) {
    Voisin * cour;
    Voisin * tmp;
    for(int i =0; i < nb; i++) {
        cour = voisins[i];
        while(cour != NULL) {
            tmp = cour;
            cour = cour->suiv;
            free(tmp);
        }
    }
    free(voisins);
}

int square(int a) {
    return a*a;
}

int max(int a, int b) {
    if(a > b) return a;
    return b;
}

int distanceEuclidienne(int courant, int fin, int p) {
    return sqrt(square(courant/p - fin/p) + square(courant%p - fin%p));
}

int distTchey(int courant, int fin, int p){
 return max(abs(courant/p - fin/p),abs(courant%p - fin%p));
}

int distManhattan(int courant,int fin, int p)
{
 return abs(courant/p - fin/p) + abs(courant%p - fin%p);
}