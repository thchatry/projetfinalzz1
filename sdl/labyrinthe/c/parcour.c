#include "../h/parcour.h"



void parcoursProfondeur(Graph * graphe, int n, int p, int depart)
{
    SDL_Window * fenetre = NULL;
    SDL_Renderer * renderer = NULL;

    int L = 1920*0.8, H = 1080*0.8,
        width = L/p, height = H/n,
        LARGEUR = width*p, HAUTEUR = height*n;

    fenetre = SDL_CreateWindow("Labyrinthe", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, LARGEUR, HAUTEUR, SDL_WINDOW_RESIZABLE);
    if (fenetre == NULL) end_sdl(0, "ERROR WINDOW CREATION", fenetre, renderer);  

    renderer = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", fenetre, renderer);

    SDL_Texture * texture = NULL;
    texture = load_texture_from_image("img/buisson.png", fenetre, renderer);


    int ** matrice = graphe_lab_vers_matrice(graphe, n, p);
    Pile ** pile = creaPile();
    int summit;
    int * coord = malloc(2 * sizeof(int));

    int ** sommet_explore = malloc(n * sizeof(int *));
    for(int i = 0; i<n; i++)
    {
        sommet_explore[i] = malloc(p * sizeof(int));
    }

    for(int i = 0; i<n; i++)
    {
        for(int j = 0; j<p; j++)
        {
            sommet_explore[i][j] = 0;
        }
    }

    empiler(pile, depart);
    summit = sommet(pile);

    while(!pileEstVide(pile))
    {
        coord = coordonnees(summit, p);

        if(!sommet_explore[coord[0]][coord[1]]) explorer(pile, matrice, sommet_explore, summit, p, coord);

        dessinLabTilesetExplore(fenetre, renderer, texture, matrice, sommet_explore, n, p);
        SDL_RenderPresent(renderer);
        SDL_Delay(20);
        
        if(summit == sommet(pile))
        {
            if(!pileEstVide(pile)) depiler(pile);
        }
        if(!pileEstVide(pile)) summit = sommet(pile);
    }


    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(fenetre);

    SDL_Quit();

    libererPile(pile);
    liberer_matrice(matrice, n);
    liberer_matrice(sommet_explore, n);
    free(coord);
}


int * coordonnees(int ind, int p)
{
    int * coord = malloc(2 * sizeof(int));

    coord[1] = ind;
    coord[0] = 0;

    while(coord[1]-p >= 0)
    {
        coord[1] -= p;
        coord[0]++;
    }

    return coord;
}

void explorer(Pile ** pile, int ** matrice, int ** explore, int sommet, int p, int * coord)
{
    explore[coord[0]][coord[1]] = 1;

    if((matrice[coord[0]][coord[1]] & 1) && explore[coord[0]-1][coord[1]] == 0)
    {
        empiler(pile, sommet - p);
    }
    if((matrice[coord[0]][coord[1]] & 2) && explore[coord[0]][coord[1]+1] == 0)
    {
        empiler(pile, sommet + 1);
    }
    if((matrice[coord[0]][coord[1]] & 4) && explore[coord[0]+1][coord[1]] == 0)
    {
        empiler(pile, sommet + p);
    }
    if((matrice[coord[0]][coord[1]] & 8) && explore[coord[0]][coord[1]-1] == 0)
    {
        empiler(pile, sommet - 1);
    }
}


void controlePetitBonhomme(SDL_Window * fenetre, Graph * graphe, int n, int p)
{
    srand(time(NULL));

    SDL_Renderer * renderer = NULL;

    SDL_bool prog_on = SDL_TRUE, end = SDL_FALSE, konami = SDL_FALSE, animation = SDL_FALSE;
     SDL_Event event;

    int N = 1, E = 2, S = 4, W = 8,
        depart = rand()%(n*p), arrivee = rand()%(n*p),
        k = 0, dir = S;  

    renderer = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", fenetre, renderer);

    SDL_Texture * textureMur = load_texture_from_image("img/buisson.png", fenetre, renderer);
    SDL_Texture * textureBonhomme = load_texture_from_image("img/frisk.png", fenetre, renderer);
    SDL_Texture * textureFin = load_texture_from_image("img/fin.png", fenetre, renderer);

    int ** matrice = graphe_lab_vers_matrice(graphe, n, p);

    int ** sommet_explore = malloc(n * sizeof(int *));
    for(int i = 0; i<n; i++)
    {
        sommet_explore[i] = malloc(p * sizeof(int));
    }

    initMatriceValeur(sommet_explore, n, p, 1);

    int summit = depart;
    int * coord = coordonnees(summit, p);
    int * finish = coordonnees(arrivee, p);

    dessinLabTilesetExplore(fenetre, renderer, textureMur, matrice, sommet_explore, n, p);
    dessinBonhommeQuiTourne(fenetre, renderer, textureBonhomme, coord, n, p, dir);
    dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
    SDL_RenderPresent(renderer);
    SDL_Delay(2000);

    while(prog_on)
    {

        while(prog_on && SDL_PollEvent(&event) && !animation)
        {
            switch(event.type)
            {
                case SDL_QUIT:
                    prog_on = SDL_FALSE;
                    break;

                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym) {
                        case SDLK_LEFT:
                            if(matrice[coord[0]][coord[1]] & W)
                            {
                                summit -= 1;
                                dir = W;
                                animation = SDL_TRUE;
                            }
                            if(k == 4 || k == 6)
                            {
                                k++;
                            }
                            else
                            {
                                k = 0;
                            }
                            break;
                        case SDLK_RIGHT:
                            if(matrice[coord[0]][coord[1]] & E)
                            {
                                summit += 1;
                                dir = E;
                                animation = SDL_TRUE;
                            }
                            if(k == 5 || k == 7)
                            {
                                k++;
                            }
                            else
                            {
                                k = 0;
                            }
                            break;
                        case SDLK_UP:
                            if(matrice[coord[0]][coord[1]] & N)
                            {
                                summit -= p;
                                dir = N;
                                animation = SDL_TRUE;
                            }
                            if(k == 0 || k == 1)
                            {
                                k++;
                            }
                            else
                            {
                                k = 0;
                            }
                            break;
                        case SDLK_DOWN:
                            if(matrice[coord[0]][coord[1]] & S)
                            {
                                summit += p;
                                dir = S;
                                animation = SDL_TRUE;
                            }
                            if(k == 2 || k == 3)
                            {
                                k++;
                            }
                            else
                            {
                                k = 0;
                            }
                            break;
                        case SDLK_b:
                            if(k == 8)
                            {
                                k++;
                            }
                            else
                            {
                                k = 0;
                            }
                            break;
                        case SDLK_a:
                            if(k == 9)
                            {
                                k++;
                            }
                            else
                            {
                                k = 0;
                            }
                            break;
                        case SDLK_s:
                            if(k == 10)
                            {
                                k++;
                            }
                            else
                            {
                                k = 0;
                            }
                            break;
                        default:
                            k = 0;
                            break;
                    }
                    break;

                default:
                    break;
            }
        }

        coord = coordonnees(summit, p);

        if(k == 11)
        {
            konami = !konami;
            k = 0;
        }

        if(konami)
        {
            initMatriceValeur(sommet_explore, n, p, 1);
        }
        else
        {
            initMatriceValeur(sommet_explore, n, p, 0);
            lampeTorche(matrice, sommet_explore, coord, dir);
        }  

        if(!end && animation)
        {
            dessinBonhommeAnimation(fenetre, renderer, textureMur, textureBonhomme, textureFin, NULL, matrice,
                                sommet_explore, coord, finish, n, p, dir);
            SDL_RenderPresent(renderer);
            SDL_Delay(25);
            animation = SDL_FALSE;
        }
        if(coord[0] == finish[0] && coord[1] == finish[1])
        {
            end = SDL_TRUE;
        }
        if(!end && !animation)
        {
            dessinLabTilesetExplore(fenetre, renderer, textureMur, matrice, sommet_explore, n, p);
            dessinBonhommeQuiTourne(fenetre, renderer, textureBonhomme, coord, n, p, dir);
            if(sommet_explore[finish[0]][finish[1]]) dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
            SDL_RenderPresent(renderer);
            SDL_Delay(25);
        }
        else
        {
            initMatriceValeur(sommet_explore, n, p, 1);
            dessinLabTilesetExplore(fenetre, renderer, textureMur, matrice, sommet_explore, n, p);
            dessinBonhommeQuiTourne(fenetre, renderer, textureBonhomme, coord, n, p, dir);
            dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
            SDL_RenderPresent(renderer);
            SDL_Delay(5000);
            prog_on = SDL_FALSE;
        }
    }

    SDL_DestroyTexture(textureMur), SDL_DestroyTexture(textureBonhomme), SDL_DestroyTexture(textureFin);
    SDL_DestroyRenderer(renderer);

    liberer_matrice(matrice, n);
    liberer_matrice(sommet_explore, n);
    free(coord);
    free(finish);
}


void lampeTorche(int ** matriceFlags, int ** matriceNoeudExplore, int * coord, int dir)
{
        int i = 0, N = 1, E = 2, S = 4, W = 8;
        
        matriceNoeudExplore[coord[0]][coord[1]] = 1;

        if(matriceFlags[coord[0]][coord[1]] & N && dir & N)
        {
            matriceNoeudExplore[coord[0]-1][coord[1]] = 1;
            if(matriceFlags[coord[0]][coord[1]] & E)
            {
                if((matriceFlags[coord[0]-1][coord[1]] & E) && (matriceFlags[coord[0]][coord[1]+1] & N)) matriceNoeudExplore[coord[0]-1][coord[1]+1] = 1;
            }
            if(matriceFlags[coord[0]][coord[1]] & W)
            {
                if((matriceFlags[coord[0]-1][coord[1]] & W) && (matriceFlags[coord[0]][coord[1]-1] & N)) matriceNoeudExplore[coord[0]-1][coord[1]-1] = 1;
            }

            i = 1;
            while(matriceFlags[coord[0]-i][coord[1]] & N)
            {
                matriceNoeudExplore[coord[0]-(i+1)][coord[1]] = 1;
                i++;
            }
        }

        if(matriceFlags[coord[0]][coord[1]] & E && dir & E)
        {
            matriceNoeudExplore[coord[0]][coord[1]+1] = 1;

            i = 1;
            while(matriceFlags[coord[0]][coord[1]+i] & E)
            {
                matriceNoeudExplore[coord[0]][coord[1]+(i+1)] = 1;
                i++;
            }
        }

        if(matriceFlags[coord[0]][coord[1]] & S && dir & S)
        {
            matriceNoeudExplore[coord[0]+1][coord[1]] = 1;
            if(matriceFlags[coord[0]][coord[1]] & E)
            {
                if((matriceFlags[coord[0]+1][coord[1]] & E) && (matriceFlags[coord[0]][coord[1]+1] & S)) matriceNoeudExplore[coord[0]+1][coord[1]+1] = 1;
            }
            if(matriceFlags[coord[0]][coord[1]] & W)
            {
                if((matriceFlags[coord[0]+1][coord[1]] & W) && (matriceFlags[coord[0]][coord[1]-1] & S)) matriceNoeudExplore[coord[0]+1][coord[1]-1] = 1;
            }

            i = 1;
            while(matriceFlags[coord[0]+i][coord[1]] & S)
            {
                matriceNoeudExplore[coord[0]+(i+1)][coord[1]] = 1;
                i++;
            }
        }

        if(matriceFlags[coord[0]][coord[1]] & W && dir & W)
        {
            matriceNoeudExplore[coord[0]][coord[1]-1] = 1;
            
            i = 1;
            while(matriceFlags[coord[0]][coord[1]-i] & W)
            {
                matriceNoeudExplore[coord[0]][coord[1]-(i+1)] = 1;
                i++;
            }
        }
}

void monstre(SDL_Window * fenetre, Graph * graphe, int n, int p)
{
    srand(time(NULL));

    SDL_Renderer * renderer = NULL;

    SDL_bool prog_on = SDL_TRUE, end = SDL_FALSE, animation = SDL_FALSE;
     SDL_Event event;

    int N = 1, E = 2, S = 4, W = 8,
        depart = rand()%(n*p), arrivee = rand()%(n*p), monstreSmart = n*p-1,
        dir = S, nbMonstre = 4, dirSmart = S;

    int * monstre = malloc(nbMonstre * sizeof(int));
    int * dirMonstre = malloc(nbMonstre * sizeof(int));

    for(int i = 0; i<nbMonstre; i++)
    {
        monstre[i] = rand()%(n*p);
        dirMonstre[i] = S;
    }

    renderer = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", fenetre, renderer);

    SDL_Texture * textureMur = load_texture_from_image("img/buisson.png", fenetre, renderer);
    SDL_Texture * textureBonhomme = load_texture_from_image("img/frisk.png", fenetre, renderer);
    SDL_Texture * textureMonstre = load_texture_from_image("img/monstre.png", fenetre, renderer);
    SDL_Texture * textureSmart = load_texture_from_image("img/smart.png", fenetre, renderer);
    SDL_Texture * textureFin = load_texture_from_image("img/fin.png", fenetre, renderer);

    int ** matrice = graphe_lab_vers_matrice(graphe, n, p);
    int ** signe = malloc(9 * sizeof(int *));
    for(int i = 0; i<9; i++)
    {
        signe[i] = malloc(4 * sizeof(int));
    }

    signe[1][0] = 1, signe[1][1] = -1, signe[1][2] = 0, signe[1][3] = 0;
    signe[2][0] = 0, signe[2][1] = 0, signe[2][2] = -1, signe[2][3] = +1;
    signe[4][0] = -1, signe[4][1] = +1, signe[4][2] = 0, signe[4][3] = 0;
    signe[8][0] = 0, signe[8][1] = 0, signe[8][2] = +1, signe[8][3] = -1;

    int summit = depart;
    int precSummit = depart;
    int * coord = coordonnees(summit, p);
    int * finish = coordonnees(arrivee, p);
    int * coordSmart = coordonnees(monstreSmart, p);
    int * predecesseur = aEtoile(graphe, depart, monstreSmart, p, 3);
    int ** coordMonstre = malloc(nbMonstre * sizeof(int * ));

    for(int i = 0; i<nbMonstre; i++)
    {
        coordMonstre[i] = coordonnees(monstre[i], p);
    }

    dessinLabTilesetExplore(fenetre, renderer, textureMur, matrice, NULL, n, p);
    dessinBonhommeQuiTourne(fenetre, renderer, textureBonhomme, coord, n, p, dir);
    dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
    dessinBonhommeQuiTourne(fenetre, renderer, textureSmart, coordSmart, n, p, dirSmart);
    for(int i = 0; i<nbMonstre; i++)
    {
        dessinBonhommeQuiTourne(fenetre, renderer, textureMonstre, coordMonstre[i], n, p, dir);
    }
    SDL_RenderPresent(renderer);

    while(prog_on)
    {
        while(prog_on && SDL_PollEvent(&event) && !animation)
        {
            switch(event.type)
            {
                case SDL_QUIT:
                    prog_on = SDL_FALSE;
                    break;

                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym) {
                        case SDLK_LEFT:
                            if(matrice[coord[0]][coord[1]] & W)
                            {
                                precSummit = summit;
                                summit -= 1;
                                dir = W;
                                animation = SDL_TRUE;
                            }
                            break;
                        case SDLK_RIGHT:
                            if(matrice[coord[0]][coord[1]] & E)
                            {
                                precSummit = summit;
                                summit += 1;
                                dir = E;
                                animation = SDL_TRUE;
                            }
                            break;
                        case SDLK_UP:
                            if(matrice[coord[0]][coord[1]] & N)
                            {
                                precSummit = summit;
                                summit -= p;
                                dir = N;
                                animation = SDL_TRUE;
                            }
                            break;
                        case SDLK_DOWN:
                            if(matrice[coord[0]][coord[1]] & S)
                            {
                                precSummit = summit;
                                summit += p;
                                dir = S;
                                animation = SDL_TRUE;
                            }
                            break;
                        default:
                            break;
                    }
                    break;

                default:
                    break;
            }
        }

        coord = coordonnees(summit, p);

        if(!end && animation)
        {
            predecesseur = aEtoile(graphe, precSummit, monstreSmart, p, 3);
            if(predecesseur[monstreSmart] - monstreSmart == -1) dirSmart = W;
            if(predecesseur[monstreSmart] - monstreSmart == 1) dirSmart = E;
            if(predecesseur[monstreSmart] - monstreSmart == -p) dirSmart = N;
            if(predecesseur[monstreSmart] - monstreSmart == p) dirSmart = S;
            monstreSmart = predecesseur[monstreSmart];
            coordSmart = coordonnees(monstreSmart, p);
            aleaMonstre(matrice, coordMonstre, monstre, dirMonstre, nbMonstre, p);
            dessinAnimation(fenetre, renderer, textureMur, textureBonhomme, textureMonstre, textureSmart, textureFin, 
                    matrice, coordMonstre, coordSmart, coord, finish,
                    dirMonstre, dirSmart, n, p, dir, nbMonstre, signe);
            SDL_RenderPresent(renderer);
            animation = SDL_FALSE;
        }
        if(coord[0] == finish[0] && coord[1] == finish[1])
        {
            end = SDL_TRUE;
        }
        for(int i = 0; i<nbMonstre; i++)
        {
            if(coord[0] == coordMonstre[i][0] && coord[1] == coordMonstre[i][1]) end = SDL_TRUE;
        }
        if(coord[0] == coordSmart[0] && coord[1] == coordSmart[1])
        {
            end = SDL_TRUE;
        }
        if(!end && !animation)
        {
            dessinLabTilesetExplore(fenetre, renderer, textureMur, matrice, NULL, n, p);
            dessinBonhommeQuiTourne(fenetre, renderer, textureBonhomme, coord, n, p, dir);
            for(int i = 0; i<nbMonstre; i++)
            {
                dessinBonhommeQuiTourne(fenetre, renderer, textureMonstre, coordMonstre[i], n, p, dirMonstre[i]);
            }
            dessinBonhommeQuiTourne(fenetre, renderer, textureSmart, coordSmart, n, p, dirSmart);
            dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
            SDL_RenderPresent(renderer);
            SDL_Delay(25);
        }
        else
        {
            dessinLabTilesetExplore(fenetre, renderer, textureMur, matrice, NULL, n, p);
            dessinBonhommeQuiTourne(fenetre, renderer, textureBonhomme, coord, n, p, dir);
            for(int i = 0; i<nbMonstre; i++)
            {
                dessinBonhommeQuiTourne(fenetre, renderer, textureMonstre, coordMonstre[i], n, p, dirMonstre[i]);
            }
            dessinBonhommeQuiTourne(fenetre, renderer, textureSmart, coordSmart, n, p, dirSmart);
            dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
            SDL_RenderPresent(renderer);
            SDL_Delay(5000);
            prog_on = SDL_FALSE;
        }
    }

    SDL_DestroyTexture(textureMur), SDL_DestroyTexture(textureBonhomme), SDL_DestroyTexture(textureFin),
    SDL_DestroyTexture(textureMonstre);
    SDL_DestroyRenderer(renderer);

    liberer_matrice(matrice, n);
    free(finish);
    free(coord);
    free(coordSmart);
    free(dirMonstre);
    free(monstre);
    free(predecesseur);

    for(int i = 0; i<nbMonstre; i++)
    {
        free(coordMonstre[i]);
    }
    free(coordMonstre);

    for(int i = 0; i<9; i++)
    {
        free(signe[i]);
    }
    free(signe);
}

void jeu()
{
    int dimension[10] = {15, 12, 14, 16, 18, 20, 22, 24, 26, 28};

    int nbNiv = 1;

    Graph ** grille = malloc(nbNiv * sizeof(Graph*));
    Graph ** labyrinthe = malloc(nbNiv * sizeof(Graph*));

    for(int i = 0; i<nbNiv; i++)
    {
        grille[i] = creerGrapheGrille(dimension[i], dimension[i]);
        fisher_yate(grille[i]);
        labyrinthe[i] = kruskalDensiteP(grille[i], 0.15);
    }

    SDL_Window * fenetre = NULL;
    int prog_on = 1;
    SDL_bool noir = SDL_FALSE, survival = SDL_FALSE;

    int L = 1920*0.8, H = 1080*0.8,
        width = L/dimension[0], height = H/dimension[0],
        LARGEUR = width*dimension[0], HAUTEUR = height*dimension[0];
    
    fenetre = SDL_CreateWindow("Labyrinthe", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, LARGEUR, HAUTEUR, SDL_WINDOW_RESIZABLE);
    if (fenetre == NULL) end_sdl(0, "ERROR WINDOW CREATION", fenetre, NULL);
    

        dessinEcranTitre(fenetre, &noir, &survival, labyrinthe[0], dimension[0], dimension[0]);
        width = L/dimension[0], height = H/dimension[0],
        LARGEUR = width*dimension[0], HAUTEUR = height*dimension[0];
        SDL_SetWindowSize(fenetre, LARGEUR, HAUTEUR);
        if(noir){controlePetitBonhomme(fenetre, labyrinthe[0], dimension[0], dimension[0]), noir = SDL_FALSE;}
        if(survival){monstre(fenetre, labyrinthe[0], dimension[0], dimension[0]), survival = SDL_FALSE;}


    
    SDL_DestroyWindow(fenetre);
    SDL_Quit();

    for(int i = 0; i<nbNiv; i++)
    {
        libererGraph(grille[i]);
    }
    free(grille), free(labyrinthe);
}

void aleaMonstre(int ** matrice, int ** coordMonstre, int * sommetMonstre, int * dirMonstre, int nbMonstre, int p)
{
    int dirDispo[4];
    int nbDir = 0;

    for(int i = 0; i<nbMonstre; i++)
    {
        coordMonstre[i] = coordonnees(sommetMonstre[i], p);
    }


    for(int i = 0; i<nbMonstre; i++)
    {
        if(matrice[coordMonstre[i][0]][coordMonstre[i][1]] & 1){dirDispo[nbDir] = 1, nbDir++;}
        if(matrice[coordMonstre[i][0]][coordMonstre[i][1]] & 2){dirDispo[nbDir] = 2, nbDir++;}
        if(matrice[coordMonstre[i][0]][coordMonstre[i][1]] & 4){dirDispo[nbDir] = 4, nbDir++;}
        if(matrice[coordMonstre[i][0]][coordMonstre[i][1]] & 8){dirDispo[nbDir] = 8, nbDir++;}

        if(nbDir != 0)
        {
            dirMonstre[i] = dirDispo[rand()%(nbDir)];
            if(dirMonstre[i] == 1) sommetMonstre[i] -= p;
            if(dirMonstre[i] == 2) sommetMonstre[i] += 1;
            if(dirMonstre[i] == 4) sommetMonstre[i] += p;
            if(dirMonstre[i] == 8) sommetMonstre[i] -= 1;

            coordMonstre[i] = coordonnees(sommetMonstre[i], p);
        }
        nbDir = 0;
    }
}