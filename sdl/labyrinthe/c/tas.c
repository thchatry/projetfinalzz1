#include "../h/tas.h"

Tas * init_tas(int t)
{
    Tas * tas = malloc(sizeof(Tas));

    tas->capacite = t;
    tas->taille = 0;
    tas->liste = malloc(t * sizeof(int));

    return tas;
}

void liberer_Tas(Tas * tas)
{
    free(tas->liste);
    free(tas);
}

void echanger(int * a, int * b)
{
    int c = *a;
    *a = *b;
    *b = c;
}

void tamiser(Tas * tas, int noeud, int n, int* distance)
{
    int k = noeud;
    int j = 2*k+1;

    
    /*
    printf("Valeur des noeuds :\n");
    printf("Valeur du noeud n : %d\n", tas->liste[k]);
    printf("Valeur di fils gauche : %d\n", tas->liste[j]);
    printf("Valeur du fils droit : %d\n", tas->liste[j+1]);
    printf("n=%d, j=%d\n", n, j);
    printf("\n");
    */
    
    
    while(j <= n)
    {
        /*
        printf("Valeur des noeuds :\n");
        printf("Valeur du noeud n : %d\n", tas->liste[k]);
        printf("Valeur di fils gauche : %d\n", tas->liste[j]);
        printf("Valeur du fils droit : %d\n", tas->liste[j+1]);
        printf("\n");
        */

        if(j < n && tas->liste[j+1] != -1 && distance[tas->liste[j]] < distance[tas->liste[j+1]])
        {
            //printf("Passage de fils gauche à droite, j=%d\n", tas->liste[j+1]);
            j++;
        }
        if(distance[tas->liste[k]] < distance[tas->liste[j]])
        {
            //printf("Echange fils %d(%d) et %d(%d)\n",k, tas->liste[k], j, tas->liste[j]);
            echanger(&(tas->liste[k]), &(tas->liste[j]));
            k = j;
            j = 2*k+1;
        }
        else
        {
            j = n+1;
        }
        //afficher(tas);
        //printf("\n");
    }
}

void construction(Tas * tas, int * contig, int tai)
{
    tas->taille = tai;
    for(int i = 0; i<tai; i++)
    {
        tas->liste[i] = contig[i];
    }

    for(int i = tai; i<tas->capacite; i++)
    {
        tas->liste[i] = -1;
    }
}

void tri_par_tas(Tas * tas, int* distance)
{
    int longueur = tas->taille;
    for(int i = longueur/2-1; i>-1; i--)
    {
        tamiser(tas, i, longueur, distance);
    }
    //afficherTas(tas);
    for(int i = longueur-1; i>0; i--)
    {
        echanger(&(tas->liste[i]), &(tas->liste[0]));
        tamiser(tas, 0, i-1, distance);
    }
    //afficherTas(tas);
}

void afficherTas(Tas * tas)
{
    printf("Nombre de noeuds : %d\n", tas->taille);
    for(int i = 0; i<tas->taille; i++)
    {
        printf("%d ", tas->liste[i]);
    }
    printf("\n");
}

int comparator(const void *p, const void *q)
{
	int firstInt = *(const int *) p;
    int secondInt = *(const int *) q;
    return firstInt-secondInt;
}

int liberer(Tas * tas) {
    
    int val = tas->liste[0];

    for(int i = 0; i < (tas->taille)-1; i++) {
        tas->liste[i] = tas->liste[i+1];
    }
    tas->taille--;
    tas->liste[tas->taille] = -1;

    return val;
}



void ajouter(Tas * tas, int nouv) {

    tas->liste[tas->taille] = nouv;
    tas->taille++;
    tas->liste[tas->taille] = -1;
}