#include "../h/dessin_lab.h"

void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer)
{
    char msg_formated[255];                                         
    int l;                                                          

    if(!ok)
    {                                                      
        strncpy(msg_formated, msg, 250);                                 
        l = strlen(msg_formated);                                        
        strcpy(msg_formated + l, " : %s\n");                     
        SDL_Log(msg_formated, SDL_GetError());                   
    }                                                               

    if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
    if (window != NULL)   SDL_DestroyWindow(window);                                        

    SDL_Quit();                                                     

    if(!ok)
    {                                                      
        exit(EXIT_FAILURE);                                              
    }                                                               
}

SDL_Texture * load_texture_from_image(char * file_image_name, SDL_Window * window, SDL_Renderer * renderer)
{
    SDL_Surface *my_image = NULL;
    SDL_Texture* my_texture = NULL;

    my_image = IMG_Load(file_image_name);

    if (my_image == NULL) end_sdl(0, "Chargement de l'image impossible", window, renderer);

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
    SDL_FreeSurface(my_image);
    if (my_texture == NULL) end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

    return my_texture;
}

SDL_Texture * load_texture_from_font(char * file_font_name, int size_font, SDL_Color color, char * text_to_show, SDL_Window * window, SDL_Renderer * renderer)
{
  SDL_Texture * texture = NULL;
  TTF_Font * font = NULL;
  SDL_Surface * surface = NULL;

  font = TTF_OpenFont(file_font_name, size_font);
  if (font == NULL) end_sdl(0, "Can't load font", window, renderer);

  surface = TTF_RenderText_Blended(font, text_to_show, color);
  if (surface == NULL) end_sdl(0, "Can't create text surface", window, renderer);

  texture = SDL_CreateTextureFromSurface(renderer, surface);
  if (texture == NULL) end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

  TTF_CloseFont(font);
  SDL_FreeSurface(surface);

  return texture;
}

void dessin_lab(int ** matrice, int a, int b)
{                  
    int N = 1, E = 2, S = 4, W = 8;

    int LARGEUR = 1920*0.8, HAUTEUR = 1080*0.8,
        width = LARGEUR/b, height = HAUTEUR/a;

    SDL_Window * fenetre = NULL;
    SDL_Renderer * renderer = NULL;  
    SDL_Point points[2]; 

    fenetre = SDL_CreateWindow("Jeu de la Vie", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, LARGEUR, HAUTEUR, SDL_WINDOW_RESIZABLE);
    if (fenetre == NULL) end_sdl(0, "ERROR WINDOW CREATION", fenetre, renderer);  

    renderer = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", fenetre, renderer);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);

    for(int i = 0; i<a; i++)
    {
        for(int j = 0; j<b; j++)
        {
            points[0].x = width/2+width*j;
            points[0].y = height/2+height*i;
            
            if(matrice[i][j] & N)
            {
                points[1].x = points[0].x;
                points[1].y = points[0].y - height;
                SDL_RenderDrawLines(renderer, points, 2);
            }

            if(matrice[i][j] & E)
            {
                points[1].x = points[0].x + width;
                points[1].y = points[0].y;
                SDL_RenderDrawLines(renderer, points, 2);
            }

            if(matrice[i][j] & S)
            {
                points[1].x = points[0].x;
                points[1].y = points[0].y + height;
                SDL_RenderDrawLines(renderer, points, 2);
            }

            if(matrice[i][j] & W)
            {
                points[1].x = points[0].x - width;
                points[1].y = points[0].y;
                SDL_RenderDrawLines(renderer, points, 2);
            }
        }
    }

    SDL_RenderPresent(renderer);
    SDL_Delay(5000);

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(fenetre);

    SDL_Quit();
}

void dessinLabTilesetExplore(SDL_Window * fenetre, SDL_Renderer* renderer, SDL_Texture * texture, int ** matrice, int ** explore, int a, int b)
{

    SDL_Rect source = {0}, dimension = {0}, destination = {0}, state = {0};


    SDL_GetWindowSize(fenetre, &dimension.w, &dimension.h);
    SDL_QueryTexture(texture, NULL, NULL, &source.w, &source.h);

    int offset = source.w / 4;

    state.w = offset;
    state.h = offset;

    destination.w = dimension.w/b;
    destination.h = dimension.h/a;

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);

    for(int i = 0; i<a; i++)
    {
        destination.y = i*destination.h;

        for(int j = 0; j<b; j++)
        {
            destination.x = j*destination.w;

            state.x = matrice[i][j];
            state.y = 0;
            while(state.x-4 >= 0)
            {
                state.x -=4;
                state.y++;
            }

            state.x = state.x * offset;
            state.y = state.y * offset;

            if( explore != NULL && explore[i][j] == 1) SDL_RenderCopy(renderer, texture, &state, &destination);
            else if(explore == NULL) SDL_RenderCopy(renderer, texture, &state, &destination);
        }
    }
}

void dessinBonhomme(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture * texture, int * coord, int n, int p)
{
    SDL_Rect source = {0}, dimension = {0}, destination = {0};

    SDL_GetWindowSize(fenetre, &dimension.w, &dimension.h);
    SDL_QueryTexture(texture, NULL, NULL, &source.w, &source.h);

    destination.w = dimension.w/p * 0.6;
    destination.h = dimension.h/n * 0.6;

    destination.x = coord[1] * dimension.w/p + (dimension.w/p - destination.w)/2;
    destination.y = coord[0] * dimension.h/n + (dimension.h/n - destination.h)/2;

    SDL_RenderCopy(renderer, texture, &source, &destination);
}

void dessinBonhommeQuiTourne(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture * texture, int * coord, int n, int p, int dir)
{
    SDL_Rect source = {0}, dimension = {0}, destination = {0}, state = {0};

    SDL_GetWindowSize(fenetre, &dimension.w, &dimension.h);
    SDL_QueryTexture(texture, NULL, NULL, &source.w, &source.h);

    int offset = source.w/4;

    destination.w = dimension.w/p * 0.5;
    destination.h = dimension.h/n * 0.5;

    destination.x = coord[1] * dimension.w/p + (dimension.w/p - destination.w)/2;
    destination.y = coord[0] * dimension.h/n + (dimension.h/n - destination.h)/2;

    state.w = offset;
    state.h = source.h;

    if(dir == 1) state.x = 3*offset;
    if(dir == 2) state.x = 2*offset;
    if(dir == 4) state.x = offset;
    if(dir == 8) state.x = 0;

    SDL_RenderCopy(renderer, texture, &state, &destination);
}

void dessinBonhommeAnimation(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture * textureMur,
                                SDL_Texture * textureBonhomme, SDL_Texture * textureFin, SDL_Texture ** textureTitre, int ** matrice,
                                int ** explore, int * coord, int * finish, int n, int p, int dir)
{
    SDL_Rect source = {0}, dimension = {0}, destination = {0}, state = {0},
                    source_titre = {0}, destination_titre = {0},
                    source_dark = {0}, destination_dark = {0},
                    source_survie = {0}, destination_survie = {0};

    SDL_GetWindowSize(fenetre, &dimension.w, &dimension.h);
    SDL_QueryTexture(textureBonhomme, NULL, NULL, &source.w, &source.h);
    
    if(textureTitre != NULL)
    {
        SDL_QueryTexture(textureTitre[0], NULL, NULL, &source_titre.w, &source_titre.h);
        SDL_QueryTexture(textureTitre[1], NULL, NULL, &source_dark.w, &source_dark.h);
        SDL_QueryTexture(textureTitre[2], NULL, NULL, &source_survie.w, &source_survie.h);
        titre(&dimension, &source_titre, &destination_titre);
        dark(&dimension, &source_dark, &destination_dark);
        survie(&dimension, &source_survie, &destination_survie);
    }

    int offset = source.w/4, delais = 2;

    destination.w = dimension.w/p * 0.5;
    destination.h = dimension.h/n * 0.5;

    state.w = offset;
    state.h = source.h;

    if(dir == 1)
    {
        if(explore != NULL) explore[coord[0]+1][coord[1]] = 1;
        state.x = 3*offset;
        destination.x = coord[1] * dimension.w/p + (dimension.w/p - destination.w)/2;
        for(int i = 0; i<101; i++)
        {
            destination.y = (float)(coord[0]+1-(i*0.01)) * dimension.h/n + (dimension.h/n - destination.h)/2;
            dessinLabTilesetExplore(fenetre, renderer, textureMur, matrice, explore, n, p);
            SDL_RenderCopy(renderer, textureBonhomme, &state, &destination);
            if(explore != NULL && explore[finish[0]][finish[1]]) dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
            else if(explore == NULL) dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
            if(textureTitre != NULL){
                SDL_RenderCopy(renderer, textureTitre[0], &source_titre, &destination_titre);
                SDL_RenderCopy(renderer, textureTitre[1], &source_dark, &destination_dark);
                SDL_RenderCopy(renderer, textureTitre[2], &source_survie, &destination_survie);
            }
            SDL_RenderPresent(renderer);
            SDL_Delay(delais);
        }
    }
    if(dir == 2)
    {
        if(explore != NULL) explore[coord[0]][coord[1]-1] = 1;
        state.x = 2*offset;
        destination.y = coord[0] * dimension.h/n + (dimension.h/n - destination.h)/2;
        for(int i = 0; i<101; i++)
        {
            destination.x = (float)(coord[1]-1+(i*0.01)) * dimension.w/p + (dimension.w/p - destination.w)/2;
            dessinLabTilesetExplore(fenetre, renderer, textureMur, matrice, explore, n, p);
            SDL_RenderCopy(renderer, textureBonhomme, &state, &destination);
            if(explore != NULL && explore[finish[0]][finish[1]]) dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
            else if(explore == NULL) dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
            if(textureTitre != NULL){
                SDL_RenderCopy(renderer, textureTitre[0], &source_titre, &destination_titre);
                SDL_RenderCopy(renderer, textureTitre[1], &source_dark, &destination_dark);
                SDL_RenderCopy(renderer, textureTitre[2], &source_survie, &destination_survie);
            }
            SDL_RenderPresent(renderer);
            SDL_Delay(delais);
        }
    }
    if(dir == 4)
    {
        if(explore != NULL) explore[coord[0]-1][coord[1]] = 1;
        state.x = offset;
        destination.x = coord[1] * dimension.w/p + (dimension.w/p - destination.w)/2;
        for(int i = 0; i<101; i++)
        {
            destination.y = (float)(coord[0]-1+(i*0.01)) * dimension.h/n + (dimension.h/n - destination.h)/2;
            dessinLabTilesetExplore(fenetre, renderer, textureMur, matrice, explore, n, p);
            SDL_RenderCopy(renderer, textureBonhomme, &state, &destination);
            if(explore != NULL && explore[finish[0]][finish[1]]) dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
            else if(explore == NULL) dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
            if(textureTitre != NULL){
                SDL_RenderCopy(renderer, textureTitre[0], &source_titre, &destination_titre);
                SDL_RenderCopy(renderer, textureTitre[1], &source_dark, &destination_dark);
                SDL_RenderCopy(renderer, textureTitre[2], &source_survie, &destination_survie);
            }
            SDL_RenderPresent(renderer);
            SDL_Delay(delais);
        }
    }
    if(dir == 8)
    {
        if(explore != NULL) explore[coord[0]][coord[1]+1] = 1;
        state.x = 0;
        destination.y = coord[0] * dimension.h/n + (dimension.h/n - destination.h)/2;
        for(int i = 0; i<101; i++)
        {
            destination.x = (float)(coord[1]+1-(i*0.01)) * dimension.w/p + (dimension.w/p - destination.w)/2;
            dessinLabTilesetExplore(fenetre, renderer, textureMur, matrice, explore, n, p);
            SDL_RenderCopy(renderer, textureBonhomme, &state, &destination);
            if(explore != NULL && explore[finish[0]][finish[1]]) dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
            else if(explore == NULL) dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
            if(textureTitre != NULL){
                SDL_RenderCopy(renderer, textureTitre[0], &source_titre, &destination_titre);
                SDL_RenderCopy(renderer, textureTitre[1], &source_dark, &destination_dark);
                SDL_RenderCopy(renderer, textureTitre[2], &source_survie, &destination_survie);
            }
            SDL_RenderPresent(renderer);
            SDL_Delay(delais);
        }
    }
}

void dessinAnimation(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture * textureMur,
                    SDL_Texture * textureBonhomme, SDL_Texture * textureMonstre, SDL_Texture * textureSmart, SDL_Texture * textureFin, 
                    int ** matrice, int ** coordMonstre, int * coordSmart, int * coord, int * finish,
                    int * dirMonstre, int dirSmart, int n, int p, int dir, int nbMonstre, int ** signe)
{
    SDL_Rect dimension = {0},
            sourceBonhomme = {0}, destinationBonhomme = {0}, stateBonhomme = {0},
            sourceMonstre = {0}, destinationSmart = {0}, stateSmart = {0};

    SDL_Rect destinationMonstre[nbMonstre], stateMonstre[nbMonstre];


    for(int i = 0; i<nbMonstre; i++)
    {
        destinationMonstre[i].x = 0;
        destinationMonstre[i].y = 0;
        destinationMonstre[i].w = 0;
        destinationMonstre[i].h = 0;
        stateMonstre[i].x = 0;
        stateMonstre[i].y = 0;
        stateMonstre[i].w = 0;
        stateMonstre[i].h = 0;
    }

    SDL_GetWindowSize(fenetre, &dimension.w, &dimension.h);
    SDL_QueryTexture(textureBonhomme, NULL, NULL, &sourceBonhomme.w, &sourceBonhomme.h);
    SDL_QueryTexture(textureMonstre, NULL, NULL, &sourceMonstre.w, &sourceMonstre.h);

    int offsetBonhomme = sourceBonhomme.w/4, offsetMonstre = sourceMonstre.w/4, delais = 2;

    destinationBonhomme.w = dimension.w/p * 0.5;
    destinationBonhomme.h = dimension.h/n * 0.5;
    stateBonhomme.w = offsetBonhomme;
    stateBonhomme.h = sourceBonhomme.h;

    destinationSmart.w = dimension.w/p * 0.5;
    destinationSmart.h = dimension.h/n * 0.5;
    stateSmart.w = offsetMonstre;
    stateSmart.h = sourceMonstre.h;

    for(int i = 0; i<nbMonstre; i++)
    {
        destinationMonstre[i].w = dimension.w/p * 0.5;
        destinationMonstre[i].h = dimension.h/n * 0.5; 
        stateMonstre[i].w = offsetMonstre;
        stateMonstre[i].h = sourceMonstre.h;
    }

    for(int i = 0; i<nbMonstre; i++)
    {
        if(dirMonstre[i] == 1) stateMonstre[i].x = 3*offsetMonstre;
        if(dirMonstre[i] == 2) stateMonstre[i].x = 2*offsetMonstre; 
        if(dirMonstre[i] == 4) stateMonstre[i].x = offsetMonstre; 
        if(dirMonstre[i] == 8) stateMonstre[i].x = 0;
    }

    if(dir == 1) stateBonhomme.x = 3*offsetBonhomme;
    if(dir == 2) stateBonhomme.x = 2*offsetBonhomme; 
    if(dir == 4) stateBonhomme.x = offsetBonhomme; 
    if(dir == 8) stateBonhomme.x = 0;

    if(dirSmart == 1) stateSmart.x = 3*offsetMonstre;
    if(dirSmart == 2) stateSmart.x = 2*offsetMonstre; 
    if(dirSmart == 4) stateSmart.x = offsetMonstre; 
    if(dirSmart == 8) stateSmart.x = 0;

    for(int i = 0; i<101; i++)
    {
        destinationBonhomme.x = (float)(coord[1]+signe[dir][2]+signe[dir][3]*(i*0.01)) * dimension.w/p + (dimension.w/p - destinationBonhomme.w)/2;
        destinationBonhomme.y = (float)(coord[0]+signe[dir][0]+signe[dir][1]*(i*0.01)) * dimension.h/n + (dimension.h/n - destinationBonhomme.h)/2;
        destinationSmart.x = (float)(coordSmart[1]+signe[dirSmart][2]+signe[dirSmart][3]*(i*0.01)) * dimension.w/p + (dimension.w/p - destinationSmart.w)/2;
        destinationSmart.y = (float)(coordSmart[0]+signe[dirSmart][0]+signe[dirSmart][1]*(i*0.01)) * dimension.h/n + (dimension.h/n - destinationSmart.h)/2;
        for(int j = 0; j<nbMonstre; j++)
        {
            destinationMonstre[j].x = (float)(coordMonstre[j][1]+signe[dirMonstre[j]][2]+signe[dirMonstre[j]][3]*(i*0.01)) * dimension.w/p + (dimension.w/p - destinationMonstre[j].w)/2;
            destinationMonstre[j].y = (float)(coordMonstre[j][0]+signe[dirMonstre[j]][0]+signe[dirMonstre[j]][1]*(i*0.01)) * dimension.h/n + (dimension.h/n - destinationMonstre[j].h)/2;
        }
        dessinLabTilesetExplore(fenetre, renderer, textureMur, matrice, NULL, n, p);
        SDL_RenderCopy(renderer, textureBonhomme, &stateBonhomme, &destinationBonhomme);
        SDL_RenderCopy(renderer, textureSmart, &stateSmart, &destinationSmart);
        for(int j = 0; j<nbMonstre; j++)
        {
            SDL_RenderCopy(renderer, textureMonstre, &stateMonstre[j], &destinationMonstre[j]);
        }
        dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
        SDL_RenderPresent(renderer);
        SDL_Delay(delais);
    }
}

void dessinEcranTitre(SDL_Window * fenetre, SDL_bool * noir, SDL_bool * survival, Graph * graphe, int n, int p) 
{

    srand(time(NULL));

    SDL_Renderer * renderer = NULL;

    SDL_bool prog_on = SDL_TRUE;
    SDL_Event event;
    SDL_Rect
            dimension = {0}, source_titre = {0}, destination_titre = {0},
            source_dark = {0}, destination_dark = {0},
            source_survie = {0}, destination_survie = {0};
    SDL_Color color = {20, 20, 20, 255};

    int N = 1, E = 2, S = 4, W = 8,
        depart = rand()%(n*p), arrivee = rand()%(n*p),
        dir = S;

    renderer = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", fenetre, renderer);

    if (TTF_Init() < 0) end_sdl(0, "Couldn't initialize SDL TTF", fenetre, renderer);

    SDL_Texture * textureMur = load_texture_from_image("img/buisson.png", fenetre, renderer);
    SDL_Texture * textureBonhomme = load_texture_from_image("img/frisk.png", fenetre, renderer);
    SDL_Texture * textureFin = load_texture_from_image("img/fin.png", fenetre, renderer);
    SDL_Texture ** textureTitre = malloc(3 * sizeof(SDL_Texture*));

    textureTitre[0] = load_texture_from_font("font/happy.ttf", 150, color, "Labyrinthe", fenetre, renderer);
    textureTitre[1] = load_texture_from_font("font/happy.ttf", 60, color, "Get back home", fenetre, renderer);
    textureTitre[2] = load_texture_from_font("font/happy.ttf", 60, color, "Survival", fenetre, renderer);


    SDL_GetWindowSize(fenetre, &dimension.w, &dimension.h);
    SDL_QueryTexture(textureTitre[0], NULL, NULL, &source_titre.w, &source_titre.h);
    SDL_QueryTexture(textureTitre[1], NULL, NULL, &source_dark.w, &source_dark.h);
    SDL_QueryTexture(textureTitre[2], NULL, NULL, &source_survie.w, &source_survie.h);
    titre(&dimension, &source_titre, &destination_titre);
    dark(&dimension, &source_dark, &destination_dark);
    survie(&dimension, &source_survie, &destination_survie);
    

    int sourisX, sourisY;

    int ** matrice = graphe_lab_vers_matrice(graphe, n, p);

    int * predecesseur = aEtoile(graphe, arrivee, depart, p, 3);

    int summit = depart;
    int * coord = coordonnees(summit, p);
    int * finish = coordonnees(arrivee, p);

    dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
    SDL_RenderCopy(renderer, textureTitre[0], &source_titre, &destination_titre);
    SDL_RenderCopy(renderer, textureTitre[1], &source_dark, &destination_dark);
    SDL_RenderCopy(renderer, textureTitre[2], &source_survie, &destination_survie);

    while(prog_on)
    {

        while(prog_on && SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_QUIT:
                    prog_on = SDL_FALSE;
                    break;

                case SDL_MOUSEBUTTONDOWN:
                    if(SDL_GetMouseState(&sourisX, &sourisY) && SDL_BUTTON(SDL_BUTTON_LEFT))
                    {
                        if( (sourisX > destination_dark.x && sourisX < destination_dark.x + destination_dark.w) && (sourisY > destination_dark.y && sourisY < destination_dark.y + destination_dark.h) )
                        {
                            *noir = SDL_TRUE;
                        }
                        if( (sourisX > destination_survie.x && sourisX < destination_survie.x + destination_survie.w) && (sourisY > destination_survie.y && sourisY < destination_survie.y + destination_survie.h) )
                        {
                            *survival = SDL_TRUE;
                        }
                    }
                break;

                default:
                    break;
            }
        }
        if(predecesseur[summit] - summit == -1) dir = W;
        if(predecesseur[summit] - summit == 1) dir = E;
        if(predecesseur[summit] - summit == -p) dir = N;
        if(predecesseur[summit] - summit == p) dir = S;
        
        summit = predecesseur[summit];
        coord = coordonnees(summit, p);

        if(finish[0] == coord[0] && finish[1] == coord[1]) {
            arrivee = rand()%(n*p);
            finish = coordonnees(arrivee, p);
            predecesseur = aEtoile(graphe, arrivee, summit, p, 3);
        }

        dessinLabTilesetExplore(fenetre, renderer, textureMur, matrice, NULL, n, p);
        dessinBonhommeAnimation(fenetre, renderer, textureMur, textureBonhomme, textureFin, textureTitre, matrice,
                                NULL, coord, finish, n, p, dir);
        dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
        SDL_RenderCopy(renderer, textureTitre[0], &source_titre, &destination_titre);
        SDL_RenderCopy(renderer, textureTitre[1], &source_dark, &destination_dark);
        SDL_RenderCopy(renderer, textureTitre[2], &source_survie, &destination_survie);
        SDL_RenderPresent(renderer);
        SDL_Delay(1);

        if(*noir) prog_on = SDL_FALSE;
        if(*survival) prog_on = SDL_FALSE;

    }

    SDL_DestroyTexture(textureMur), SDL_DestroyTexture(textureBonhomme), 
    SDL_DestroyTexture(textureFin);

    for(int i = 0; i<3; i++)
    {
        SDL_DestroyTexture(textureTitre[i]);
    }
    free(textureTitre);

    SDL_DestroyRenderer(renderer);

    liberer_matrice(matrice, n);
    free(coord);
    free(finish);

    TTF_Quit();
}

void titre(SDL_Rect * dimension, SDL_Rect * source, SDL_Rect * destination)
{
  destination->x = dimension->w/2 - source->w/2;
  destination->y = dimension->h/4 - source->h/2;
  destination->w = source->w;
  destination->h = source->h;
}

void dark(SDL_Rect * dimension, SDL_Rect * source, SDL_Rect * destination)
{
  destination->x = dimension->w/2 - source->w/2;
  destination->y = 2*dimension->h/3 - 3*source->h/2;
  destination->w = source->w;
  destination->h = source->h;
}

void survie(SDL_Rect * dimension, SDL_Rect * source, SDL_Rect * destination)
{
  destination->x = dimension->w/2 - source->w/2;
  destination->y = 2*dimension->h/3 - source->h/2;
  destination->w = source->w;
  destination->h = source->h;
}

