#include "../h/graphviz.h"

int crea_graphviz_partition(int * partition, int n, char * nom_png)
{
    int sys, nb = 0;

    for(int i = 0; i<n; i++)
    {
        if(partition[i] != i) nb++;
    }

    char nom[n];
    char commande[100];

    Agraph_t * mon_graph;
    Agnode_t ** noeud = malloc(n * sizeof(Agnode_t*));
    Agedge_t ** arrete = malloc(nb * sizeof(Agedge_t*));

    FILE * fichier;
    fichier = fopen("graph.dot", "w");
    if(fichier == NULL)
    {
        fprintf(stderr, "Impossible d'ouvrir le fichier");
        exit(EXIT_FAILURE);
    }

    GVC_t * graph_context;
    graph_context = gvContext();
    if(graph_context == NULL)
    {
        fprintf(stderr, "Impossible de créer le contexte de graph");
        exit(EXIT_FAILURE);
    }

    mon_graph = agopen("Mon Graph", Agundirected, 0);

    for(int i = 0; i<n; i++)
    {
        sprintf(nom, "%d", i);
        noeud[i] = agnode(mon_graph, nom, 1);
    }

    for(int i = 0; i<n; i++)
    {
        if(partition[i] != i) arrete[i] = agedge(mon_graph, noeud[i], noeud[partition[i]], NULL, 1);
    }

    agwrite(mon_graph, stdout);
    gvLayout(graph_context, mon_graph, "dot");
    gvRender(graph_context, mon_graph, "dot", fichier);

    sprintf(commande, "dot -Tpng demo.dot -o %s", nom_png);

    sys = system(commande);
    if (sys != 0)
    {
        fprintf(stderr, "Impossible de lancer la commande : dot -Tpng demo.dot -o graph.png");
    }

    free(noeud);
    free(arrete);
    gvFreeLayout(graph_context, mon_graph);
    agclose(mon_graph);
    fclose(fichier);
    return EXIT_SUCCESS;
}

int crea_graphviz_graphe(Graph * graph, char * nom_png)
{
    int sys;

    char nom[100];
    char commande[100];
    char valuation[10];

    Agraph_t * mon_graph;
    Agnode_t ** noeud = malloc(graph->nbNoeuds * sizeof(Agnode_t*));
    Agedge_t ** arrete = malloc(graph->nbArcs * sizeof(Agedge_t*));

    FILE * fichier;
    fichier = fopen("demo.dot", "w");
    if(fichier == NULL)
    {
        fprintf(stderr, "Impossible d'ouvrir le fichier");
        exit(EXIT_FAILURE);
    }

    GVC_t * graph_context;
    graph_context = gvContext();
    if(graph_context == NULL)
    {
        fprintf(stderr, "Impossible de créer le contexte de graph");
        exit(EXIT_FAILURE);
    }

    mon_graph = agopen("Mon Graph", Agundirected, 0);

    for(int i = 0; i<graph->nbNoeuds; i++)
    {
        sprintf(nom, "%d", i);
        noeud[i] = agnode(mon_graph, nom, 1);
    }

    for(int i = 0; i<graph->nbArcs; i++)
    {
        if(graph->arcs[i]->sommet_1 != graph->arcs[i]->sommet_2)
        {
            arrete[i] = agedge(mon_graph, noeud[graph->arcs[i]->sommet_1], noeud[graph->arcs[i]->sommet_2], NULL, 1);
            sprintf(valuation, "%d", graph->arcs[i]->poid);
            agsafeset(arrete[i], "label", valuation, "");
        }
    }
    if(!strcmp(nom_png, "labyrinthe.png")) agsafeset(mon_graph, "nodesep", "1.5", "");

    agwrite(mon_graph, stdout);
    gvLayout(graph_context, mon_graph, "dot");
    gvRender(graph_context, mon_graph, "dot", fichier);

    sprintf(commande, "dot -Tpng demo.dot -o %s", nom_png);

    sys = system(commande);
    if (sys != 0)
    {
        fprintf(stderr, "Impossible de lancer la commande : dot -Tpng demo.dot -o graph.png");
    }

    free(noeud);
    free(arrete);
    gvFreeLayout(graph_context, mon_graph);
    agclose(mon_graph);
    fclose(fichier);
    return EXIT_SUCCESS;
}