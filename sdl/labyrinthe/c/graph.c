#include "../h/graph.h"



Graph * creerGraphe(int nbNoeuds, int nbArcs)
{
    srand(time(NULL));

    Graph * graphe = malloc(sizeof(Graph));
    graphe->nbNoeuds = nbNoeuds;
    graphe->arcs = malloc(nbArcs * sizeof(Arc *));

    for(int i = 0; i<nbArcs; i++)
    {
        ajoutArc(graphe, rand()%nbNoeuds, rand()%nbNoeuds, rand()%nbNoeuds);
    }

    return graphe;
}

Graph * creerGrapheGrille(int n, int p)
{
    int nb_arretes = n*(p-1)+p*(n-1);
    Graph * labyrinthe = malloc(sizeof(Graph));
    labyrinthe->nbNoeuds = n*p;
    labyrinthe->arcs = malloc(nb_arretes*sizeof(Arc*));
    srand(time(NULL));

    for(int i = 0; i<n; i++)
    {
        for(int j = 0; j<p-1; j++)
        {
            ajoutArc(labyrinthe, p*i+j, p*i+j+1, 1 + rand()%10);
        }
    }
    for(int i = 0; i<n-1; i++)
    {
        for(int j = 0; j<p; j++)
        {
            ajoutArc(labyrinthe, p*i+j, p*i+j+p, 1 + rand()%10);
        }
    }

    return labyrinthe;
}


void ajoutArc(Graph* graph, int sommet_1, int sommet_2, int poid) {
    graph->nbArcs++;
    //graph->arcs = realloc(graph->arcs, (graph->nbArcs)*sizeof(Arc*));
    //if(graph->arcs == NULL) exit(1);
    graph->arcs[graph->nbArcs-1] = creerArc(sommet_1, sommet_2, poid);
}


Arc* creerArc(int a, int b, int poid) {
    Arc* arc = malloc(sizeof(Arc));
    arc->sommet_1 = a;
    arc->sommet_2 = b;
    arc->poid = poid;

    return arc;
}

void libererGraph(Graph* graph) {
    for(int i = 0; i < graph->nbArcs; i++) {
        free(graph->arcs[i]);
    }
    free(graph);
}

void afficherListeArrete(Graph* graph) {
    for(int i = 0; i < graph->nbArcs; i++) {
        printf("%d -> %d, %d\n", graph->arcs[i]->sommet_1, graph->arcs[i]->sommet_2, graph->arcs[i]->poid);
    }
    printf("\n");
}


void afficheTab(int* tab, int n) {

    printf("indice\t");
    for(int i = 0; i < n; i++) {
        printf("\t%d", i);
    }

    printf("\nvaleur\t");
    for(int i = 0; i < n; i++) {
        printf("\t%d", tab[i]);
    }
    printf("\n\n");
}