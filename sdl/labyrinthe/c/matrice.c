#include "../h/matrice.h"


int ** graphe_lab_vers_matrice(Graph * labyrinthe, int a, int b)
{
    int x = 0, y = 0;

    int ** matrice = malloc(a*sizeof(int*));
    for(int i = 0; i<a; i++)
    {
        matrice[i] = malloc(b*sizeof(int));
    }
    
    for(int i = 0; i<a; i++)
    {
        for(int j = 0; j<b; j++)
        {
            matrice[i][j] = 0;
        }
    }
    
    for(int i = 0; i<labyrinthe->nbArcs; i++)
    {
        x = 0;
        y = labyrinthe->arcs[i]->sommet_1;
        while(y-b >= 0)
        {
            y = y-b;
            x++;
        }

        if((labyrinthe->arcs[i]->sommet_1)+1 == labyrinthe->arcs[i]->sommet_2)
        {
            matrice[x][y] += 2;
            matrice[x][y+1] += 8;
        }
        if((labyrinthe->arcs[i]->sommet_1)+b == labyrinthe->arcs[i]->sommet_2)
        {
            matrice[x][y] += 4;
            matrice[x+1][y] += 1;
        }
    }
    

    return matrice;
}


void afficheMatrice(int n, int m, int** matrice) {
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < m; j++) {
            printf("%d\t", matrice[i][j]);
        }
        printf("\n");
    }
}

void liberer_matrice(int ** matrice, int n)
{
    for(int i = 0; i<n; i++)
    {
        free(matrice[i]);
    }
    free(matrice);
}


void initMatriceValeur(int ** matriceFlags, int n, int p, int valeur)
{
    for(int i = 0; i<n; i++)
    {
        for(int j = 0; j<p; j++)
        {
            matriceFlags[i][j] = valeur;
        }
    }
}