#include <SDL2/SDL.h>
#include <stdio.h>


int HAUTEUR = 700;
int LARGEUR = 1400;
int COLONNE = 48;
int LIGNE = 24;

int R = 255;
int G = 255;
int B = 255;



void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                  char const* msg,                                    // message à afficher
                  SDL_Window* window,                                 // fenêtre à fermer
                  SDL_Renderer* renderer) {                           // renderer à fermer
  char msg_formated[255];                                         
  int l;                                                          

  if (!ok) {                                                      
         strncpy(msg_formated, msg, 250);                                 
         l = strlen(msg_formated);                                        
         strcpy(msg_formated + l, " : %s\n");                     

         SDL_Log(msg_formated, SDL_GetError());                   
  }                                                               

  if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
  if (window != NULL)   SDL_DestroyWindow(window);                                        

  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
}

void draw(int matrice[COLONNE][LIGNE], SDL_Renderer* renderer) {

    int color;

    for(int i = 0; i < COLONNE; i++) {
        for(int j = 0; j < LIGNE; j++) {


            SDL_Rect rectangle;  

            if(matrice[i][j] == 0) {
                color = 1;
            } else {
                color = 0;
            }

            SDL_SetRenderDrawColor(renderer,                                
                              color*R, color*G, color*B,                               // mode Red, Green, Blue (tous dans 0..255)
                              255); 

            rectangle.x = (LARGEUR/COLONNE)*i;                                                    // x haut gauche du rectangle
            rectangle.y = (HAUTEUR/LIGNE)*j;                                                    // y haut gauche du rectangle
            rectangle.w = LARGEUR/COLONNE;                                                  // sa largeur (w = width)
            rectangle.h = HAUTEUR/LIGNE;                                                  // sa hauteur (h = height)

            SDL_RenderFillRect(renderer, &rectangle);
        }
    }
    

}

int nbVoisin(int matrice[COLONNE][LIGNE], int col, int ligne) {
    // Sommme des 8 cases entourant (col,ligne)
    int nb = 
        matrice[(col-1 + COLONNE)%COLONNE][(ligne-1 + LIGNE)%LIGNE] +
        matrice[(col-1 + COLONNE)%COLONNE][ligne] + 
        matrice[(col-1 + COLONNE)%COLONNE][(ligne+1)%LIGNE] +
        matrice[col][(ligne-1+ LIGNE)%LIGNE] +
        matrice[col][(ligne+1)%LIGNE] + 
        matrice[(col+1)%COLONNE][(ligne-1+ LIGNE)%LIGNE] +
        matrice[(col+1)%COLONNE][ligne] +
        matrice[(col+1)%COLONNE][(ligne+1)%LIGNE];

    return nb;
}

void game(SDL_Renderer* renderer) {

    // Initialisation de la matrice de jeu et de la matrice des voisins
    int matrice[COLONNE][LIGNE];
    int matriceVoisin[COLONNE][LIGNE];

    // regles[0], regles de naissance
    // regles[1], regles de survie
    int regles[2][9] = {{0, 0, 0, 1, 0, 0, 0, 0, 0},{0, 0, 1, 1,  0, 0, 0, 0, 0}};

    // initialisaiton: tout est mort
    for(int i = 0; i < COLONNE; i++) {
        for (int j = 0; j < LIGNE; j++) {
            matrice[i][j] = 0;
        }
    }

    // Dessin initial: deux vaisseaux
    matrice[7][7] = 1;
    matrice[7][8] = 1;
    matrice[6][8] = 1;
    matrice[5][8] = 1;
    matrice[6][6] = 1;

    matrice[17][7] = 1;
    matrice[17][8] = 1;
    matrice[16][8] = 1;
    matrice[15][8] = 1;
    matrice[16][6] = 1;
    
    // affichage
    draw(matrice, renderer);
    SDL_RenderPresent(renderer); 
    SDL_Delay(250);

    // Boucle de jeu
    SDL_Event event;
    SDL_bool program_on = SDL_TRUE;
    SDL_bool pause = SDL_FALSE;
    int x, y, add = 0;
    while (program_on) {

        // Boucle des evenements
        while(program_on && SDL_PollEvent(&event)){      

            switch(event.type){                       
            case SDL_QUIT :                          
                program_on = SDL_FALSE;                 
                break;

            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {             // la touche appuyée est ...
                case SDLK_SPACE:                            // 'SPC'
                    pause = !pause;
                    break;
                case SDLK_ESCAPE:                           // 'ESCAPE'  
                    break;
                case SDLK_q:                                // 'q'
                    program_on = 0;                           // 'escape' ou 'q', d'autres façons de quitter le programme                                     
                    break;
                
                default:                                    // Une touche appuyée qu'on ne traite pas
                    break;
                }
                break;
                
            case SDL_MOUSEBUTTONDOWN:
                if (SDL_GetMouseState(&x, &y) & SDL_BUTTON(SDL_BUTTON_LEFT) ) {         // Si c'est un click gauche
                    add = 1;      
                } else if (SDL_GetMouseState(&x, &y) & SDL_BUTTON(SDL_BUTTON_RIGHT) ) { // Si c'est un click droit
                    add = 1;     
                }
                break;
            default:                                  
                break;
            }
        }

        if(!pause) {

            // Calcul de tout les voisins
            for(int i = 0; i < COLONNE; i++) {
                for (int j = 0; j < LIGNE; j++) {
                    matriceVoisin[i][j] = nbVoisin(matrice, i, j);
                    
                }
            }

            // Calcul des nouvelles cellules
            for(int i = 0; i < COLONNE; i++) {
                for (int j = 0; j < LIGNE; j++) {
                    matrice[i][j] = regles[matrice[i][j]][matriceVoisin[i][j]];
                }
            }
            
            

            // affichage
            draw(matrice, renderer);
            SDL_RenderPresent(renderer);                                        // affichage

            // Pause exprimée en ms
            SDL_Delay(100);                                                    
        } else if (add) {
            x = x*((float)COLONNE/LARGEUR);             // Calcul de la case d'insertion
            y = y*((float)LIGNE/HAUTEUR);
            matrice[x][y] = !matrice[x][y];             // Ivertion de la case: mort -> vivant, vivant -> mort
            draw(matrice, renderer);                    // Affichage
            SDL_RenderPresent(renderer);
            add = 0;
        }
    }


    
}


int main(int argc, char **argv) {
    (void)argc;
    (void)argv;

    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;

    window = SDL_CreateWindow("Jeu de la vie",
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, 
                            LARGEUR,
                            HAUTEUR,
                            SDL_WINDOW_OPENGL);

    if (window == NULL) {
        end_sdl(0, "ERROR WINDOW CREATION", window, renderer);
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    /********************************************************************************************************************/

    game(renderer);

    /*********************************************************************************************************************/
    /* on referme proprement la SDL */
    end_sdl(1, "Normal ending", window, renderer);
    return EXIT_SUCCESS;
}
