
#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define LARGEUR 1330
#define HAUTEUR 1080 




void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                  char const* msg,                                    // message à afficher
                  SDL_Window* window,                                 // fenêtre à fermer
                  SDL_Renderer* renderer) {                           // renderer à fermer
  char msg_formated[255];                                         
  int l;                                                          

  if (!ok) {                                                      
         strncpy(msg_formated, msg, 250);                                 
         l = strlen(msg_formated);                                        
         strcpy(msg_formated + l, " : %s\n");                     

         SDL_Log(msg_formated, SDL_GetError());                   
  }                                                               

  if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
  if (window != NULL)   SDL_DestroyWindow(window);                                        

  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
}      


void draw(SDL_Renderer* renderer) {                                  
        
 SDL_Rect tab[10];

 for(int i=0;i<10;i++)  // 10 premiers rectangles qui constituent le serpent
 {
   SDL_SetRenderDrawColor(renderer, rand()%255, rand()%255, rand()%255, 255); // couleur random
   tab[i].x= i*40;                 // ecarté de 40 p
   tab[i].y= 100;
   tab[i].h= 20;
   tab[i].w= 40;

   SDL_RenderDrawRect(renderer, &tab[i]);

   SDL_RenderPresent(renderer);

   SDL_Delay(100); 
 }

 for(int j=0;j<100;j++)  // supression du rectangle de queue et ajout  du rectangle de tete
 {
   SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
   SDL_RenderDrawRect(renderer, &tab[j%10]);       // peint en noir le rectangle en queue
   SDL_RenderPresent(renderer);

  SDL_Delay(10); 


   SDL_SetRenderDrawColor(renderer, rand()%255, rand()%255, rand()%255, 255);
   tab[j%10].x=(tab[(j+9)%10].x + 40)%(LARGEUR);  // replace en tete le rectangle qui etait en queue
   tab[j%10].y=100;
   tab[j%10].h=20;
   tab[j%10].w=40;

   SDL_RenderDrawRect(renderer, &tab[j%10]);

   SDL_RenderPresent(renderer);

   SDL_Delay(100); 

  

 }

}





int main(int argc, char** argv) {
  (void)argc;
  (void)argv;

  SDL_Window* window = NULL;
  SDL_Renderer* renderer = NULL;

  SDL_DisplayMode screen;

/*********************************************************************************************************************/  
/*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
  if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);

  SDL_GetCurrentDisplayMode(0, &screen);
  printf("Résolution écran\n\tw : %d\n\th : %d\n", screen.w,
              screen.h);

  /* Création de la fenêtre */
  window = SDL_CreateWindow("Premier dessin",
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, screen.w *0.7 ,
                            screen.h*0.7 ,
                            SDL_WINDOW_OPENGL);
  if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

  /* Création du renderer */
  renderer = SDL_CreateRenderer(
           window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

  /*********************************************************************************************************************/
  /*                                     On dessine dans le renderer                                                   */
  draw(renderer);                                                     // appel de la fonction qui crée l'image  
  SDL_RenderPresent(renderer);                                        // affichage
  SDL_Delay(3000);                                                    // pause
 

  /*********************************************************************************************************************/
  /* on referme proprement la SDL */
  end_sdl(1, "Normal ending", window, renderer);
  return EXIT_SUCCESS;
}
