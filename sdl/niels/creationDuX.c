#include <SDL2/SDL.h>
#include <stdio.h>

/************************************/
/*  exemple de création de fenêtres */
/************************************/

int NB = 30;


int main(int argc, char **argv) {

  (void)argc;
  (void)argv;

  SDL_Window* tab[NB];               // Future fenêtres

  SDL_DisplayMode dm;  
  SDL_GetDesktopDisplayMode(0, &dm);
  int Width = dm.w;
  int Height = dm.h;
  printf("w = %d, h = %d\n", Width, Height);

  /* Initialisation de la SDL  + gestion de l'échec possible */
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());      // l'initialisation de la SDL a échoué 
    exit(EXIT_FAILURE);
  }

  /* Création des fenetres */
  for(int i = 0; i < NB; i++) {

    tab[i] = SDL_CreateWindow(
        "Fenêtre X",                    
        (1900/NB)*i, (1080/NB)*i,                                 
        400, 300,                              
        SDL_WINDOW_RESIZABLE);                
  
    if (tab[i] == NULL) {
        SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError());   // échec de la création de la fenêtre
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
  }


    for(int i = 0; i< NB; i++) {

    tab[i] = SDL_CreateWindow(
        "Fenêtre X",                    
        1900 -(1900/NB)*i,1080 -(1080/NB)*i,                            
        400, 300,                              
        SDL_WINDOW_RESIZABLE);                
  
    if (tab[i] == NULL) {
        SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError());   // échec de la création de la fenêtre
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    }

    
 

  /* Normalement, on devrait ici remplir les fenêtres... */
  SDL_Delay(2000);                         // Pause exprimée  en ms

  /* et on referme tout ce qu'on a ouvert en ordre inverse de la création */
  for(int i = 0; i < NB; i++) {
        SDL_DestroyWindow(tab[i]);
    }

    SDL_Quit();
    return 0;
}

