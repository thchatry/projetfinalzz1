#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

int HAUTEUR = 700;
int LARGEUR = 1400;
int NB_POINTS = 200;


void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                  char const* msg,                                    // message à afficher
                  SDL_Window* window,                                 // fenêtre à fermer
                  SDL_Renderer* renderer) {                           // renderer à fermer
  char msg_formated[255];                                         
  int l;                                                          

  if (!ok) {                                                      
         strncpy(msg_formated, msg, 250);                                 
         l = strlen(msg_formated);                                        
         strcpy(msg_formated + l, " : %s\n");                     

         SDL_Log(msg_formated, SDL_GetError());                   
  }                                                               

  if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
  if (window != NULL)   SDL_DestroyWindow(window);                                        

  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
}                                                                 

void draw(SDL_Renderer* renderer) {                                   
    SDL_Rect rectangle;                                             
    SDL_Point points[NB_POINTS];

    /* tracer un cercle n'est en fait pas trivial, voilà le résultat sans algo intelligent ... */
    for (float angle = 0; angle < 2 * M_PI; angle += M_PI / 50) {  

        SDL_SetRenderDrawColor(renderer,                                
                                255, 255, 255,                               // mode Red, Green, Blue (tous dans 0..255)
                                255);                                   // 0 = transparent ; 255 = opaque
        rectangle.x = 0;                                                    // x haut gauche du rectangle
        rectangle.y = 0;                                                    // y haut gauche du rectangle
        rectangle.w = LARGEUR;                                                  // sa largeur (w = width)
        rectangle.h = HAUTEUR;                                                  // sa hauteur (h = height)

        SDL_RenderFillRect(renderer, &rectangle);

        SDL_SetRenderDrawColor(renderer,
                                0, 0, 0,                          //          de bleu
                                255);                                 // opaque
        SDL_RenderDrawLine(renderer, 
                            LARGEUR/2, HAUTEUR/2,
                            LARGEUR/2 + 300 * cos(angle),                  // coordonnée en x
                            HAUTEUR/2 + 300 * sin(angle));                 //            en y   
        
        SDL_RenderPresent(renderer);
        SDL_Delay(25);
    }


    int longueur = 200;
    int base[2] = {100, 100};

    for (float angle = 0; angle < 8 * M_PI; angle += M_PI / 25) {

        SDL_SetRenderDrawColor(renderer,                                
                                255, 255, 255,                               // mode Red, Green, Blue (tous dans 0..255)
                                255);                                   // 0 = transparent ; 255 = opaque
        rectangle.x = 0;                                                    // x haut gauche du rectangle
        rectangle.y = 0;                                                    // y haut gauche du rectangle
        rectangle.w = LARGEUR;                                                  // sa largeur (w = width)
        rectangle.h = HAUTEUR;                                                  // sa hauteur (h = height)

        SDL_RenderFillRect(renderer, &rectangle);
        SDL_SetRenderDrawColor(renderer,
                                0, 0, 0,                          //          de bleu
                                255); 


        for(int i = 0; i < NB_POINTS; i++) {
            
            int x = base[0] + (longueur/NB_POINTS)*i;
            int y = base[1] + cos((x*2*M_PI)/100 + angle)*10;

            points[i].x = x;
            points[i].y = y;

            
        }
        base[0] = base[0] + 8;
        SDL_RenderDrawPoints(renderer, points, NB_POINTS);
        SDL_RenderPresent(renderer);
        SDL_Delay(25);
    }

}

int main(int argc, char** argv) {
  (void)argc;
  (void)argv;

  SDL_Window* window = NULL;
  SDL_Renderer* renderer = NULL;

  SDL_DisplayMode screen;

/*********************************************************************************************************************/  
/*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
  if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);

  SDL_GetCurrentDisplayMode(0, &screen);
  printf("Résolution écran\n\tw : %d\n\th : %d\n", screen.w,
              screen.h);

  /* Création de la fenêtre */
  window = SDL_CreateWindow("Premier dessin",
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED,
                            LARGEUR,
                            HAUTEUR,
                            SDL_WINDOW_OPENGL);
  if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

  /* Création du renderer */
  renderer = SDL_CreateRenderer(
           window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

  /*********************************************************************************************************************/
  /*                                     On dessine dans le renderer                                                   */
  draw(renderer);                                                     // appel de la fonction qui crée l'image  
  SDL_RenderPresent(renderer);                                        // affichage
  SDL_Delay(1000);                                                    // Pause exprimée en ms

  /*********************************************************************************************************************/
  /* on referme proprement la SDL */
  end_sdl(1, "Normal ending", window, renderer);
  return EXIT_SUCCESS;
}