#include <SDL2/SDL.h>
#include <stdio.h>

/************************************/
/*  exemple de création de fenêtres */
/************************************/

int NB = 20;


int main(int argc, char **argv) {
  (void)argc;
  (void)argv;

  int hauteur = 1600;
  int largeur = 900;

  SDL_Window* windowsGauche[NB];               // Future fenêtres
  SDL_Window* windowsDroite[NB];               // Future fenêtres


  /* Initialisation de la SDL  + gestion de l'échec possible */
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());      // l'initialisation de la SDL a échoué 
    exit(EXIT_FAILURE);
  }

  /* Création des fenetres */
  for(int i = 0; i < NB; i++) {

    windowsGauche[i] = SDL_CreateWindow(
        "Fenêtre X",                    // codage en utf8, donc accents possibles
        (hauteur/NB)*i, (largeur/NB)*i,                                  // coin haut gauche en haut gauche de l'écran
        200, 200,                              // largeur = 400, hauteur = 300
        SDL_WINDOW_RESIZABLE);                 // redimensionnable

    if (windowsGauche[i] == NULL) {
        SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError());   // échec de la création de la fenêtre
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    windowsDroite[i] = SDL_CreateWindow(
        "Fenêtre X",                    // codage en utf8, donc accents possibles
        hauteur - (hauteur/NB)*i, (largeur/NB)*i,                                  // coin haut gauche en haut gauche de l'écran
        200, 200,                              // largeur = 400, hauteur = 300
        SDL_WINDOW_RESIZABLE);                 // redimensionnable

    if (windowsDroite[i] == NULL) {
        SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError());   // échec de la création de la fenêtre
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
  }

  SDL_DisplayMode screen;

  SDL_GetCurrentDisplayMode(0, &screen);
  printf("Résolution écran\n\tw : %d\n\th : %d\n", 
                screen.w,
              screen.h);
 

  /* Normalement, on devrait ici remplir les fenêtres... */
  SDL_Delay(2000);                         // Pause exprimée  en ms

    int x;
    int y;

  for(int i =0; i < NB; i++) {
    SDL_GetWindowPosition(windowsGauche[i],
                                 &x, &y);
    SDL_SetWindowPosition(windowsGauche[i],
                                x-100, y-100);
  }

  SDL_Delay(2000);

  /* et on referme tout ce qu'on a ouvert en ordre inverse de la création */
  for(int i = NB-1; i >= 0 ; i--) {
        SDL_DestroyWindow(windowsGauche[i]);
        SDL_DestroyWindow(windowsDroite[i]);
    }

    SDL_Quit();
    return 0;
}