#include <SDL2/SDL.h>
#include <stdio.h>
#include <SDL2/SDL_image.h>

int HAUTEUR = 700;
int LARGEUR = 1400;
int FPS = 60;


void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
            char const* msg,                                    // message à afficher
            SDL_Window* window,                                 // fenêtre à fermer
            SDL_Renderer* renderer) {                           // renderer à fermer
    char msg_formated[255];                                         
    int l;                                                          

    if (!ok) {                                                      
    strncpy(msg_formated, msg, 250);                                 
    l = strlen(msg_formated);                                        
    strcpy(msg_formated + l, " : %s\n");                     

    SDL_Log(msg_formated, SDL_GetError());                   
    }                                                               

    if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
    if (window != NULL)   SDL_DestroyWindow(window);                                        

    SDL_Quit();                                                     

    if (!ok) {                                                      
    exit(EXIT_FAILURE);                                              
  }                                                               
}

void play_with_texture_1(SDL_Texture *my_texture, SDL_Window *window,
                         SDL_Renderer *renderer) {
  SDL_Rect 
          source = {0},                         // Rectangle définissant la zone de la texture à récupérer
          window_dimensions = {0},              // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
          destination = {0};                    // Rectangle définissant où la zone_source doit être déposée dans le renderer

  SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);                    // Récupération des dimensions de la fenêtre
  SDL_QueryTexture(my_texture, NULL, NULL,
                   &source.w, &source.h);       // Récupération des dimensions de l'image

  destination = window_dimensions;              // On fixe les dimensions de l'affichage à  celles de la fenêtre

  /* On veut afficher la texture de façon à ce que l'image occupe la totalité de la fenêtre */

  SDL_RenderCopy(renderer, my_texture,
                 &source,
                 &destination);                 // Création de l'élément à afficher
}

void play_with_texture_2(SDL_Texture* my_texture,
                            SDL_Texture* bg_texture,
                            SDL_Window* window,
                            SDL_Renderer* renderer) {
    SDL_Rect 
        source = {0},                      // Rectangle définissant la zone de la texture à récupérer
        window_dimensions = {0},           // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
        destination = {0};                 // Rectangle définissant où la zone_source doit être déposée dans le renderer

    SDL_GetWindowSize(
        window, &window_dimensions.w,
        &window_dimensions.h);               // Récupération des dimensions de la fenêtre

    window_dimensions.w = LARGEUR;

    SDL_QueryTexture(my_texture, NULL, NULL,
                    &source.w, &source.h);  // Récupération des dimensions de l'image


    int nb_images = 32;                         //  Il y a 8 vignette dans la ligne qui nous intéresse
    float zoom = 1;                             // zoom, car ces images sont un peu petites
    int offset_x = source.w / 8,                // La largeur d'une vignette de l'image
        offset_y = source.h / 4;                // La hauteur d'une vignette de l'image
    SDL_Rect state[32];   

    /* construction des différents rectangles autour de chacune des vignettes de la planche */
    int i = 0;                                   
    for (int y = 0; y < source.h ; y += offset_y) {
        for (int x = 0; x < source.w; x += offset_x) {
            state[i].x = x;
            state[i].y = y;
            state[i].w = offset_x;
            state[i].h = offset_y;
            ++i;
        }
    }


    int origine[2] = {((window_dimensions.w - destination.w) /2), 100};
    destination.w = 100;         // La destination est un zoom de la source
    destination.h = 100;         // La destination est un zoom de la source
    destination.x = origine[0];
    destination.y = origine[1];

    SDL_RenderCopy(renderer, my_texture,     // Préparation de l'affichage  
                &source,
                &destination);     

    SDL_Event event; 
    i = 0;
    int vitesseX = 12;
    int vitesseY = 0;
    float gravite = 2;
    float rebound = 1.06;   //[1; 1,1], 1.06 stabilite,

    SDL_bool program_on = SDL_TRUE;
    while (program_on) {

        // Boucle des evenements
        while(program_on && SDL_PollEvent(&event)){      

            switch(event.type){                       
            case SDL_QUIT :                          
                program_on = SDL_FALSE;                 
                break;

            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {             // la touche appuyée est ...
                case SDLK_p:                                // 'p'
                    vitesseX = -vitesseX;
                case SDLK_SPACE:                            // 'SPC'
                    break;
                case SDLK_ESCAPE:                           // 'ESCAPE'  
                case SDLK_q:                                // 'q'
                    program_on = 0;                           // 'escape' ou 'q', d'autres façons de quitter le programme                                     
                    break;
                default:                                    // Une touche appuyée qu'on ne traite pas
                    break;
                }
            default:                                  
                break;
            }
        }

        destination.x += vitesseX;
        destination.y += vitesseY;

        if(destination.x >= 1300 || destination.x <= 0) {
            vitesseX = -vitesseX;
        }

        if(destination.y >= 600) {
            destination.y = 600;
            vitesseY = -vitesseY * rebound;
        }



        vitesseY += gravite;

        //play_with_texture_1(bg_texture, window, renderer);

        SDL_RenderCopy(renderer,                  // Préparation de l'affichage
                my_texture, &state[i], &destination);
        i = (i + 1) % nb_images;                  // Passage à l'image suivante, le modulo car l'animation est cyclique 
        SDL_RenderPresent(renderer);             
        SDL_Delay(1000/FPS);
        SDL_RenderClear(renderer);                        
    }       

    SDL_RenderClear(renderer);               // Effacer la fenêtre
}




int main(int argc, char **argv) {



    (void)argc;
    (void)argv;

    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;

    window = SDL_CreateWindow("Texture",
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, 
                            LARGEUR,
                            HAUTEUR,
                            SDL_WINDOW_OPENGL);

    if (window == NULL) {
        end_sdl(0, "ERROR WINDOW CREATION", window, renderer);
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);


    SDL_Texture* my_texture; 
    SDL_Texture* bg_texture;
    SDL_Texture* ball_texture;

    my_texture = IMG_LoadTexture(renderer,"./img/ballRotation.png");
    if (my_texture == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);
    bg_texture = IMG_LoadTexture(renderer,"./img/olive.jpg");
    if (bg_texture == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);
    ball_texture = IMG_LoadTexture(renderer,"./img/football.jpg");
    if (ball_texture == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);

    play_with_texture_2(my_texture, bg_texture, window, renderer);


    SDL_DestroyTexture(my_texture);
    SDL_DestroyTexture(bg_texture);
    end_sdl(1, "Normal ending", window, renderer);
    IMG_Quit();
}