#include "partition_arbo.h"

graphe_t * crea_graphe(int n, int nb)
{
    srand(time(NULL));

    graphe_t * graphe = malloc(sizeof(graphe_t));
    graphe->nb_noeud = n;
    graphe->liste_arrete = malloc(nb * sizeof(int *));
    for(int i = 0; i<nb; i++)
    {
        graphe->liste_arrete[i] = malloc(3 * sizeof(int));
    }

    for(int i = 0; i<nb; i++)
    {
        graphe->liste_arrete[i][0] = rand()%n;
        graphe->liste_arrete[i][1] = rand()%n;
        graphe->liste_arrete[i][2] = rand()%n;
    }

    return graphe;
}

void crea_graphe_graphe(graphe_t * graphe, int * partition, int * hauteur, int nb)
{
    for(int i = 0; i<nb; i++)
    {
        fusion(graphe->liste_arrete[i][0], graphe->liste_arrete[i][1], partition, hauteur);
    }
}

void liberer_graphe(graphe_t * graphe, int nb)
{
    for(int i = 0; i<nb; i++)
    {
        free(graphe->liste_arrete[i]);
    }
    free(graphe->liste_arrete);
    free(graphe);
}

void afficher_liste_arrete(graphe_t * graphe, int nb)
{
    for(int i = 0; i<nb; i++)
    {
        printf("%d ", graphe->liste_arrete[i][0]);
    }
    printf("\n");
    for(int i = 0; i<nb; i++)
    {
        printf("%d ", graphe->liste_arrete[i][1]);
    }
    printf("\n");
    for(int i = 0; i<nb; i++)
    {
        printf("%d ", graphe->liste_arrete[i][2]);
    }
    printf("\n\n");
}