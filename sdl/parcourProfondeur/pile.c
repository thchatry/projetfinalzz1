#include "pile.h"

Pile ** creaPile()
{
    Pile ** pile = malloc(sizeof(Pile *));
    
    if(pile == NULL)
    {
        printf("Erreur lors de la création de la pile\n");
        exit(1);
    }
    *pile = NULL;

    return pile;
}

void libererPile(Pile ** pile)
{
    Pile * pointeur = *pile;
    Pile * memoire;
    while(pointeur != NULL){
        memoire = pointeur;
        pointeur = pointeur -> suivant;
        free(memoire);
    }
    free(pile);
}

void empiler(Pile ** pile, int empilade)
{
    Pile * nouveau = malloc(sizeof(Pile));
    nouveau->element = empilade;
    nouveau->suivant = *pile;
    *pile = nouveau;
}

void depiler(Pile ** pile)
{
    if(!pileEstVide(pile)){
        Pile * memoire = *pile;
        *pile = (*pile)->suivant;
        free(memoire);
    }
}

int sommet(Pile ** pile)
{
    return (*pile)->element;
}

int pileEstVide(Pile ** pile)
{
    int res = 0;
    if(*pile == NULL){
        res = 1;
    }
    return(res);
}