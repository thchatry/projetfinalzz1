#include "partition_arbo.h"

graphe_t * crea_graphe_grille(int n, int p)
{
    int nb_arretes = n*(p-1)+p*(n-1), ind = 0;
    graphe_t * labyrinthe = malloc(sizeof(graphe_t));
    labyrinthe->nb_noeud = n*p;
    labyrinthe->liste_arrete = malloc(nb_arretes*sizeof(int*));
    for(int i = 0; i<nb_arretes; i++)
    {
        labyrinthe->liste_arrete[i] = malloc(3 * sizeof(int));
    }

    srand(time(NULL));

    for(int i = 0; i<n; i++)
    {
        for(int j = 0; j<p-1; j++)
        {
            labyrinthe->liste_arrete[ind][0] = p*i+j;
            labyrinthe->liste_arrete[ind][1] = p*i+j+1;
            labyrinthe->liste_arrete[ind][2] = 1 + rand()%10;
            ind++;
        }
    }
    for(int i = 0; i<n-1; i++)
    {
        for(int j = 0; j<p; j++)
        {
            labyrinthe->liste_arrete[ind][0] = p*i+j;
            labyrinthe->liste_arrete[ind][1] = p*i+j+p;
            labyrinthe->liste_arrete[ind][2] = 1 + rand()%10;
            ind++;
        }
    }

    return labyrinthe;
}

int ** graphe_lab_vers_matrice(graphe_t * labyrinthe, int a, int b)
{
    int c = a*(b-1)+b*(a-1), 
        x = 0, y = 0;

    int ** matrice = malloc(a*sizeof(int*));
    for(int i = 0; i<a; i++)
    {
        matrice[i] = malloc(b*sizeof(int));
    }
    
    for(int i = 0; i<a; i++)
    {
        for(int j = 0; j<b; j++)
        {
            matrice[i][j] = 0;
        }
    }
    
    for(int i = 0; i<c; i++)
    {
        x = 0;
        y = labyrinthe->liste_arrete[i][0];
        while(y-b >= 0)
        {
            y = y-b;
            x++;
        }

        if((labyrinthe->liste_arrete[i][0])+1 == labyrinthe->liste_arrete[i][1])
        {
            matrice[x][y] += 2;
            matrice[x][y+1] += 8;
        }
        if((labyrinthe->liste_arrete[i][0])+b == labyrinthe->liste_arrete[i][1])
        {
            matrice[x][y] += 4;
            matrice[x+1][y] += 1;
        }
    }
    

    return matrice;
}
