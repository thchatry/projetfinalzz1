#include "partition_arbo.h"
#include "pile.h"

void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer)
{
    char msg_formated[255];                                         
    int l;                                                          

    if(!ok)
    {                                                      
        strncpy(msg_formated, msg, 250);                                 
        l = strlen(msg_formated);                                        
        strcpy(msg_formated + l, " : %s\n");                     
        SDL_Log(msg_formated, SDL_GetError());                   
    }                                                               

    if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
    if (window != NULL)   SDL_DestroyWindow(window);                                        

    SDL_Quit();                                                     

    if(!ok)
    {                                                      
        exit(EXIT_FAILURE);                                              
    }                                                               
}

SDL_Texture * load_texture_from_image(char * file_image_name, SDL_Window * window, SDL_Renderer * renderer)
{
    SDL_Surface *my_image = NULL;
    SDL_Texture* my_texture = NULL;

    my_image = IMG_Load(file_image_name);

    if (my_image == NULL) end_sdl(0, "Chargement de l'image impossible", window, renderer);

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
    SDL_FreeSurface(my_image);
    if (my_texture == NULL) end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

    return my_texture;
}

void dessin_lab(int ** matrice, int a, int b)
{                  
    int nb_noeud = a*b,
        N = 1, E = 2, S = 4, W = 8;

    int LARGEUR = 1920*0.8, HAUTEUR = 1080*0.8,
        width = LARGEUR/b, height = HAUTEUR/a;

    SDL_Window * fenetre = NULL;
    SDL_Renderer * renderer = NULL;  
    SDL_Point points[2]; 

    fenetre = SDL_CreateWindow("Jeu de la Vie", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, LARGEUR, HAUTEUR, SDL_WINDOW_RESIZABLE);
    if (fenetre == NULL) end_sdl(0, "ERROR WINDOW CREATION", fenetre, renderer);  

    renderer = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", fenetre, renderer);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);

    for(int i = 0; i<a; i++)
    {
        for(int j = 0; j<b; j++)
        {
            points[0].x = width/2+width*j;
            points[0].y = height/2+height*i;
            
            if(matrice[i][j] & N)
            {
                points[1].x = points[0].x;
                points[1].y = points[0].y - height;
                SDL_RenderDrawLines(renderer, points, 2);
            }

            if(matrice[i][j] & E)
            {
                points[1].x = points[0].x + width;
                points[1].y = points[0].y;
                SDL_RenderDrawLines(renderer, points, 2);
            }

            if(matrice[i][j] & S)
            {
                points[1].x = points[0].x;
                points[1].y = points[0].y + height;
                SDL_RenderDrawLines(renderer, points, 2);
            }

            if(matrice[i][j] & W)
            {
                points[1].x = points[0].x - width;
                points[1].y = points[0].y;
                SDL_RenderDrawLines(renderer, points, 2);
            }
        }
    }

    SDL_RenderPresent(renderer);
    SDL_Delay(5000);

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(fenetre);

    SDL_Quit();
}

void dessin_lab_tileset(SDL_Window * fenetre, SDL_Renderer* renderer, SDL_Texture * texture, int ** matrice, int ** explore, int a, int b)
{
    int nb_noeud = a*b;

    SDL_Rect source = {0}, dimension = {0}, destination = {0}, state = {0};

    SDL_GetWindowSize(fenetre, &dimension.w, &dimension.h);
    SDL_QueryTexture(texture, NULL, NULL, &source.w, &source.h);

    int offset = source.w / 4;

    state.w = offset;
    state.h = offset;

    destination.w = dimension.w/b;
    destination.h = dimension.h/a;

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);

    for(int i = 0; i<a; i++)
    {
        destination.y = i*destination.h;

        for(int j = 0; j<b; j++)
        {
            destination.x = j*destination.w;

            state.x = matrice[i][j];
            state.y = 0;
            while(state.x-4 >= 0)
            {
                state.x -=4;
                state.y++;
            }

            state.x = state.x * offset;
            state.y = state.y * offset;

            if(explore[i][j] == 1) SDL_RenderCopy(renderer, texture, &state, &destination);

        }
    }
}

void dessinBonhomme(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture * texture, int * coord, int n, int p)
{
    SDL_Rect source = {0}, dimension = {0}, destination = {0};

    SDL_GetWindowSize(fenetre, &dimension.w, &dimension.h);
    SDL_QueryTexture(texture, NULL, NULL, &source.w, &source.h);

    destination.w = dimension.w/p * 0.6;
    destination.h = dimension.h/n * 0.6;

    destination.x = coord[1] * dimension.w/p + (dimension.w/p - destination.w)/2;
    destination.y = coord[0] * dimension.h/n + (dimension.h/n - destination.h)/2;

    SDL_RenderCopy(renderer, texture, &source, &destination);
}

void dessinBonhommeQuiTourne(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture * texture, int * coord, int n, int p, int dir)
{
    SDL_Rect source = {0}, dimension = {0}, destination = {0}, state = {0};

    SDL_GetWindowSize(fenetre, &dimension.w, &dimension.h);
    SDL_QueryTexture(texture, NULL, NULL, &source.w, &source.h);

    int offset = source.w/4;

    destination.w = dimension.w/p * 0.5;
    destination.h = dimension.h/n * 0.5;

    destination.x = coord[1] * dimension.w/p + (dimension.w/p - destination.w)/2;
    destination.y = coord[0] * dimension.h/n + (dimension.h/n - destination.h)/2;

    state.w = offset;
    state.h = source.h;

    if(dir == 1) state.x = 3*offset;
    if(dir == 2) state.x = 2*offset;
    if(dir == 4) state.x = offset;
    if(dir == 8) state.x = 0;

    SDL_RenderCopy(renderer, texture, &state, &destination);
}

void dessinBonhommeAnimation(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture * textureMur,
                                SDL_Texture * textureBonhomme,SDL_Texture * textureFin, int ** matrice,
                                int ** explore, int * coord, int * finish, int n, int p, int dir)
{
    SDL_Rect source = {0}, dimension = {0}, destination = {0}, state = {0};

    SDL_GetWindowSize(fenetre, &dimension.w, &dimension.h);
    SDL_QueryTexture(textureBonhomme, NULL, NULL, &source.w, &source.h);

    int offset = source.w/4, delais = 2;

    destination.w = dimension.w/p * 0.5;
    destination.h = dimension.h/n * 0.5;

    state.w = offset;
    state.h = source.h;

    if(dir == 1)
    {
        explore[coord[0]+1][coord[1]] = 1;
        state.x = 3*offset;
        destination.x = coord[1] * dimension.w/p + (dimension.w/p - destination.w)/2;
        for(int i = 0; i<101; i++)
        {
            destination.y = (float)(coord[0]+1-(i*0.01)) * dimension.h/n + (dimension.h/n - destination.h)/2;
            dessin_lab_tileset(fenetre, renderer, textureMur, matrice, explore, n, p);
            SDL_RenderCopy(renderer, textureBonhomme, &state, &destination);
            if(explore[finish[0]][finish[1]]) dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
            SDL_RenderPresent(renderer);
            SDL_Delay(delais);
        }
    }
    if(dir == 2)
    {
        explore[coord[0]][coord[1]-1] = 1;
        state.x = 2*offset;
        destination.y = coord[0] * dimension.h/n + (dimension.h/n - destination.h)/2;
        for(int i = 0; i<101; i++)
        {
            destination.x = (float)(coord[1]-1+(i*0.01)) * dimension.w/p + (dimension.w/p - destination.w)/2;
            dessin_lab_tileset(fenetre, renderer, textureMur, matrice, explore, n, p);
            SDL_RenderCopy(renderer, textureBonhomme, &state, &destination);
            if(explore[finish[0]][finish[1]]) dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
            SDL_RenderPresent(renderer);
            SDL_Delay(delais);
        }
    }
    if(dir == 4)
    {
        explore[coord[0]-1][coord[1]] = 1;
        state.x = offset;
        destination.x = coord[1] * dimension.w/p + (dimension.w/p - destination.w)/2;
        for(int i = 0; i<101; i++)
        {
            destination.y = (float)(coord[0]-1+(i*0.01)) * dimension.h/n + (dimension.h/n - destination.h)/2;
            dessin_lab_tileset(fenetre, renderer, textureMur, matrice, explore, n, p);
            SDL_RenderCopy(renderer, textureBonhomme, &state, &destination);
            if(explore[finish[0]][finish[1]]) dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
            SDL_RenderPresent(renderer);
            SDL_Delay(delais);
        }
    }
    if(dir == 8)
    {
        explore[coord[0]][coord[1]+1] = 1;
        state.x = 0;
        destination.y = coord[0] * dimension.h/n + (dimension.h/n - destination.h)/2;
        for(int i = 0; i<101; i++)
        {
            destination.x = (float)(coord[1]+1-(i*0.01)) * dimension.w/p + (dimension.w/p - destination.w)/2;
            dessin_lab_tileset(fenetre, renderer, textureMur, matrice, explore, n, p);
            SDL_RenderCopy(renderer, textureBonhomme, &state, &destination);
            if(explore[finish[0]][finish[1]]) dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
            SDL_RenderPresent(renderer);
            SDL_Delay(delais);
        }
    }
}

void parcours_profondeur(graphe_t * graphe, int n, int p, int depart)
{
    SDL_Window * fenetre = NULL;
    SDL_Renderer * renderer = NULL;

    int L = 1920*0.8, H = 1080*0.8,
        width = L/p, height = H/n,
        LARGEUR = width*p, HAUTEUR = height*n;

    fenetre = SDL_CreateWindow("Labyrinthe", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, LARGEUR, HAUTEUR, SDL_WINDOW_RESIZABLE);
    if (fenetre == NULL) end_sdl(0, "ERROR WINDOW CREATION", fenetre, renderer);  

    renderer = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", fenetre, renderer);

    SDL_Texture * textureMur = load_texture_from_image("buisson.png", fenetre, renderer);
    SDL_Texture * textureBonhomme = load_texture_from_image("bonhomme.png", fenetre, renderer);


    int ** matrice = graphe_lab_vers_matrice(graphe, n, p);
    Pile ** pile = creaPile();
    int summit;
    int * coord = malloc(2 * sizeof(int));

    int ** sommet_explore = malloc(n * sizeof(int *));
    for(int i = 0; i<n; i++)
    {
        sommet_explore[i] = malloc(p * sizeof(int));
    }

    for(int i = 0; i<n; i++)
    {
        for(int j = 0; j<p; j++)
        {
            sommet_explore[i][j] = 0;
        }
    }

    empiler(pile, depart);
    summit = sommet(pile);

    while(!pileEstVide(pile))
    {
        coord = coordonnees(summit, p);

        if(!sommet_explore[coord[0]][coord[1]]) explorer(pile, matrice, sommet_explore, summit, p, coord);

        dessin_lab_tileset(fenetre, renderer, textureMur, matrice, sommet_explore, n, p);
        dessinBonhomme(fenetre, renderer, textureBonhomme, coord, n, p);
        SDL_RenderPresent(renderer);
        SDL_Delay(25);

        if(summit == sommet(pile))
        {
            depiler(pile);
        }
        if(!pileEstVide(pile)) summit = sommet(pile);
    }

    SDL_DestroyTexture(textureMur), SDL_DestroyTexture(textureBonhomme);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(fenetre);

    SDL_Quit();

    libererPile(pile);
    liberer_matrice(matrice, n);
    liberer_matrice(sommet_explore, n);
    free(coord);
}

int * coordonnees(int ind, int p)
{
    int * coord = malloc(2 * sizeof(int));

    coord[1] = ind;
    coord[0] = 0;

    while(coord[1]-p >= 0)
    {
        coord[1] -= p;
        coord[0]++;
    }

    return coord;
}

void explorer(Pile ** pile, int ** matrice, int ** explore, int sommet, int p, int * coord)
{
    explore[coord[0]][coord[1]] = 1;

    int ind = 0;

    if((matrice[coord[0]][coord[1]] & 1) && explore[coord[0]-1][coord[1]] == 0)
    {
        empiler(pile, sommet - p);
        ind++;
        empiler(pile, sommet);
    }
    if((matrice[coord[0]][coord[1]] & 2) && explore[coord[0]][coord[1]+1] == 0)
    {
        empiler(pile, sommet + 1);
        ind++;
        empiler(pile, sommet);
    }
    if((matrice[coord[0]][coord[1]] & 4) && explore[coord[0]+1][coord[1]] == 0)
    {
        empiler(pile, sommet + p);
        ind++;
        empiler(pile, sommet);
    }
    if((matrice[coord[0]][coord[1]] & 8) && explore[coord[0]][coord[1]-1] == 0)
    {
        empiler(pile, sommet - 1);
        ind++;
        empiler(pile, sommet);
    }
    if(ind) depiler(pile);
}

void dansLeNoir(graphe_t * graphe, int n, int p)
{
    srand(time(NULL));

    SDL_Window * fenetre = NULL;
    SDL_Renderer * renderer = NULL;

    SDL_bool prog_on = SDL_TRUE, end = SDL_FALSE, konami = SDL_FALSE, animation = SDL_FALSE;
     SDL_Event event;

    int L = 1920*0.8, H = 1080*0.8,
        width = L/p, height = H/n,
        LARGEUR = width*p, HAUTEUR = height*n,
        N = 1, E = 2, S = 4, W = 8,
        depart = rand()%(n*p), arrivee = rand()%(n*p),
        k = 0, dir = S;

    fenetre = SDL_CreateWindow("Labyrinthe", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, LARGEUR, HAUTEUR, SDL_WINDOW_RESIZABLE);
    if (fenetre == NULL) end_sdl(0, "ERROR WINDOW CREATION", fenetre, renderer);  

    renderer = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", fenetre, renderer);

    SDL_Texture * textureMur = load_texture_from_image("buisson.png", fenetre, renderer);
    SDL_Texture * textureBonhomme = load_texture_from_image("frisk.png", fenetre, renderer);
    SDL_Texture * textureFin = load_texture_from_image("fin.png", fenetre, renderer);

    int ** matrice = graphe_lab_vers_matrice(graphe, n, p);

    int ** sommet_explore = malloc(n * sizeof(int *));
    for(int i = 0; i<n; i++)
    {
        sommet_explore[i] = malloc(p * sizeof(int));
    }

    initMatriceValeur(sommet_explore, n, p, 1);

    int summit = depart;
    int * coord = coordonnees(summit, p);
    int * finish = coordonnees(arrivee, p);

    dessin_lab_tileset(fenetre, renderer, textureMur, matrice, sommet_explore, n, p);
    dessinBonhommeQuiTourne(fenetre, renderer, textureBonhomme, coord, n, p, dir);
    dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
    SDL_RenderPresent(renderer);
    SDL_Delay(2000);

    while(prog_on)
    {

        while(prog_on && SDL_PollEvent(&event) && !animation)
        {
            switch(event.type)
            {
                case SDL_QUIT:
                    prog_on = SDL_FALSE;
                    break;

                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym) {
                        case SDLK_LEFT:
                            if(matrice[coord[0]][coord[1]] & W)
                            {
                                summit -= 1;
                                dir = W;
                                animation = SDL_TRUE;
                            }
                            if(k == 4 || k == 6)
                            {
                                k++;
                            }
                            else
                            {
                                k = 0;
                            }
                            break;
                        case SDLK_RIGHT:
                            if(matrice[coord[0]][coord[1]] & E)
                            {
                                summit += 1;
                                dir = E;
                                animation = SDL_TRUE;
                            }
                            if(k == 5 || k == 7)
                            {
                                k++;
                            }
                            else
                            {
                                k = 0;
                            }
                            break;
                        case SDLK_UP:
                            if(matrice[coord[0]][coord[1]] & N)
                            {
                                summit -= p;
                                dir = N;
                                animation = SDL_TRUE;
                            }
                            if(k == 0 || k == 1)
                            {
                                k++;
                            }
                            else
                            {
                                k = 0;
                            }
                            break;
                        case SDLK_DOWN:
                            if(matrice[coord[0]][coord[1]] & S)
                            {
                                summit += p;
                                dir = S;
                                animation = SDL_TRUE;
                            }
                            if(k == 2 || k == 3)
                            {
                                k++;
                            }
                            else
                            {
                                k = 0;
                            }
                            break;
                        case SDLK_b:
                            if(k == 8)
                            {
                                k++;
                            }
                            else
                            {
                                k = 0;
                            }
                            break;
                        case SDLK_a:
                            if(k == 9)
                            {
                                k++;
                            }
                            else
                            {
                                k = 0;
                            }
                            break;
                        case SDLK_s:
                            if(k == 10)
                            {
                                k++;
                            }
                            else
                            {
                                k = 0;
                            }
                            break;
                        default:
                            k = 0;
                            break;
                    }
                    break;

                default:
                    break;
            }
        }

        coord = coordonnees(summit, p);

        if(k == 11)
        {
            konami = !konami;
            k = 0;
        }

        if(konami)
        {
            initMatriceValeur(sommet_explore, n, p, 1);
        }
        else
        {
            initMatriceValeur(sommet_explore, n, p, 0);
            lampeTorche(matrice, sommet_explore, coord, dir);
        } 


        if(!end && animation)
        {
            dessinBonhommeAnimation(fenetre, renderer, textureMur, textureBonhomme, textureFin, matrice, sommet_explore, coord, finish, n, p, dir);
            SDL_RenderPresent(renderer);
            SDL_Delay(25);
            animation = SDL_FALSE;
        }
        if(coord[0] == finish[0] && coord[1] == finish[1])
        {
            end = SDL_TRUE;
        } 
        if(!end && !animation)
        {
            dessin_lab_tileset(fenetre, renderer, textureMur, matrice, sommet_explore, n, p);
            dessinBonhommeQuiTourne(fenetre, renderer, textureBonhomme, coord, n, p, dir);
            if(sommet_explore[finish[0]][finish[1]]) dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
            SDL_RenderPresent(renderer);
            SDL_Delay(25);
        }
        else
        {
            initMatriceValeur(sommet_explore, n, p, 1);
            dessin_lab_tileset(fenetre, renderer, textureMur, matrice, sommet_explore, n, p);
            dessinBonhommeQuiTourne(fenetre, renderer, textureBonhomme, coord, n, p, dir);
            dessinBonhomme(fenetre, renderer, textureFin, finish, n, p);
            SDL_RenderPresent(renderer);
            SDL_Delay(5000);
            prog_on = SDL_FALSE;
        }
    }

    SDL_DestroyTexture(textureMur), SDL_DestroyTexture(textureBonhomme), SDL_DestroyTexture(textureFin);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(fenetre);

    SDL_Quit();
    liberer_matrice(matrice, n);
    liberer_matrice(sommet_explore, n);
    free(coord);
    free(finish);
}

void initMatriceValeur(int ** matriceFlags, int n, int p, int valeur)
{
    for(int i = 0; i<n; i++)
    {
        for(int j = 0; j<p; j++)
        {
            matriceFlags[i][j] = valeur;
        }
    }
}

void lampeTorche(int ** matriceFlags, int ** matriceNoeudExplore, int * coord, int dir)
{
        int i = 0, N = 1, E = 2, S = 4, W = 8;
        
        matriceNoeudExplore[coord[0]][coord[1]] = 1;

        if(matriceFlags[coord[0]][coord[1]] & N && dir & N)
        {
            matriceNoeudExplore[coord[0]-1][coord[1]] = 1;
            if(matriceFlags[coord[0]][coord[1]] & E)
            {
                if((matriceFlags[coord[0]-1][coord[1]] & E) && (matriceFlags[coord[0]][coord[1]+1] & N)) matriceNoeudExplore[coord[0]-1][coord[1]+1] = 1;
            }
            if(matriceFlags[coord[0]][coord[1]] & W)
            {
                if((matriceFlags[coord[0]-1][coord[1]] & W) && (matriceFlags[coord[0]][coord[1]-1] & N)) matriceNoeudExplore[coord[0]-1][coord[1]-1] = 1;
            }

            i = 1;
            while(matriceFlags[coord[0]-i][coord[1]] & N)
            {
                matriceNoeudExplore[coord[0]-(i+1)][coord[1]] = 1;
                i++;
            }
        }

        if(matriceFlags[coord[0]][coord[1]] & E && dir & E)
        {
            matriceNoeudExplore[coord[0]][coord[1]+1] = 1;

            i = 1;
            while(matriceFlags[coord[0]][coord[1]+i] & E)
            {
                matriceNoeudExplore[coord[0]][coord[1]+(i+1)] = 1;
                i++;
            }
        }

        if(matriceFlags[coord[0]][coord[1]] & S && dir & S)
        {
            matriceNoeudExplore[coord[0]+1][coord[1]] = 1;
            if(matriceFlags[coord[0]][coord[1]] & E)
            {
                if((matriceFlags[coord[0]+1][coord[1]] & E) && (matriceFlags[coord[0]][coord[1]+1] & S)) matriceNoeudExplore[coord[0]+1][coord[1]+1] = 1;
            }
            if(matriceFlags[coord[0]][coord[1]] & W)
            {
                if((matriceFlags[coord[0]+1][coord[1]] & W) && (matriceFlags[coord[0]][coord[1]-1] & S)) matriceNoeudExplore[coord[0]+1][coord[1]-1] = 1;
            }

            i = 1;
            while(matriceFlags[coord[0]+i][coord[1]] & S)
            {
                matriceNoeudExplore[coord[0]+(i+1)][coord[1]] = 1;
                i++;
            }
        }

        if(matriceFlags[coord[0]][coord[1]] & W && dir & W)
        {
            matriceNoeudExplore[coord[0]][coord[1]-1] = 1;
            
            i = 1;
            while(matriceFlags[coord[0]][coord[1]-i] & W)
            {
                matriceNoeudExplore[coord[0]][coord[1]-(i+1)] = 1;
                i++;
            }
        }
}