#ifndef _PARTITION_ARBO_H_
#define _PARTITION_ARBO_H_

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <graphviz/gvc.h>
#include <graphviz/cgraph.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>

#include "pile.h"

typedef struct graphe{
    int nb_noeud;
    int ** liste_arrete;
} graphe_t;

int * tableau_hauteur(int valeur);
int * creer(int valeur);
int recuperer_classe(int a, int * partition);
void fusion(int a, int b, int * partition, int * hauteur);
void lister_classe(int a, int * partition, int valeur);
void lister(int * partition, int valeur, int * hauteur);
void afficher_tab(int * partition, int valeur);
graphe_t * crea_graphe(int n, int nb);
void crea_graphe_graphe(graphe_t * graphe, int * partition, int * hauteur, int nb);
void liberer_graphe(graphe_t * graphe, int nb);
int ** matrice_adja(int n);
void liberer_matrice(int ** matrice, int n);
void crea_graphe_matrice(int ** matrice, int * partition, int * hauteur, int n);
void afficher_matrice(int ** matrice, int n, int p);
void tri_arrete(graphe_t * graphe, int nb);
void echanger(int * a, int * b);
void afficher_liste_arrete(graphe_t * graphe, int nb);
graphe_t * kruskal(graphe_t * graphe, int * partition, int * hauteur, int n, int nb);
void fisher_yate(graphe_t * graphe, int nb);
int crea_graphviz_partition(int * partition, int n, char * nom_png);
int crea_graphviz_graphe(graphe_t * graphe, int nb, char * nom_png);
graphe_t * crea_graphe_grille(int n, int p);

int ** graphe_lab_vers_matrice(graphe_t * labyrinthe, int a, int b);

void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer);
SDL_Texture * load_texture_from_image(char * file_image_name, SDL_Window * window, SDL_Renderer * renderer);
void dessin_lab(int ** matrice, int a, int b);
void dessin_lab_tileset(SDL_Window * fenetre, SDL_Renderer* renderer, SDL_Texture * texture, int ** matrice, int ** explore, int a, int b);
void dessinBonhomme(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture * texture, int * coord, int n, int p);
void dessinBonhommeQuiTourne(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture * texture, int * coord, int n, int p, int dir);
void dessinBonhommeAnimation(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture * textureMur,
                                SDL_Texture * textureBonhomme,SDL_Texture * textureFin, int ** matrice,
                                int ** explore, int * coord, int * finish, int n, int p, int dir);

graphe_t * kruskal_p(graphe_t * graphe, int * partition, int * hauteur, int n, int nb, float p);

void parcours_profondeur(graphe_t * graphe, int n, int p, int depart);
int * coordonnees(int ind, int p);
void explorer(Pile ** pile, int ** matrice, int ** explore, int sommet, int p, int * coord);

void dansLeNoir(graphe_t * graphe, int n, int p);

void initMatriceValeur(int ** matrice, int n, int p, int valeur);

void lampeTorche(int ** matriceFlags, int ** matriceNoeudExplore, int * coord, int dir);
#endif