#include "partition_arbo.h"

void echanger(int * a, int * b)
{
    int c = *a;
    *a = *b;
    *b = c;
}

void tri_arrete(graphe_t * graphe, int nb)
{
    int min = 100, ind = 0;

    for(int i = 0; i<nb; i++)
    {
        for(int j = i; j<nb; j++)
        {
            if(graphe->liste_arrete[j][2] < min)
            {
                min = graphe->liste_arrete[j][2];
                ind = j;
            }
        }

        for(int k = 0; k<3; k++)
        {
            echanger(&(graphe->liste_arrete[i][k]), &(graphe->liste_arrete[ind][k]));
        }

        min = 100;
        ind = i+1;
    }
}

void fisher_yate(graphe_t * graphe, int nb)
{
    srand(time(NULL));
    int random;
    for(int i = nb-1; i>-1; i--)
    {
        random = rand()%(i+1);
        for(int k = 0; k<3; k++)
        {
            echanger(&(graphe->liste_arrete[random][k]), &(graphe->liste_arrete[i][k]));
        }
    }
}

graphe_t * kruskal(graphe_t * graphe, int * partition, int * hauteur, int n, int nb)
{
    int ind = 0;

    graphe_t * minimal = malloc(sizeof(graphe_t));
    minimal->nb_noeud = n;
    minimal->liste_arrete = malloc(nb * sizeof(int *));
    for(int i = 0; i<nb; i++)
    {
        minimal->liste_arrete[i] = malloc(3 * sizeof(int));
    }

    for(int i = 0; i<nb; i++)
    {
        if(recuperer_classe(graphe->liste_arrete[i][0], partition) != recuperer_classe(graphe->liste_arrete[i][1], partition))
        {
            fusion(graphe->liste_arrete[i][0], graphe->liste_arrete[i][1], partition, hauteur);
            minimal->liste_arrete[ind][0] = graphe->liste_arrete[i][0];
            minimal->liste_arrete[ind][1] = graphe->liste_arrete[i][1];
            minimal->liste_arrete[ind][2] = graphe->liste_arrete[i][2];
            ind++;
        }
    }
    return minimal;
}

graphe_t * kruskal_p(graphe_t * graphe, int * partition, int * hauteur, int n, int nb, float p)
{
    srand(time(NULL));

    int ind = 0;

    float alpha = 0;

    graphe_t * minimal = malloc(sizeof(graphe_t));
    minimal->nb_noeud = n;
    minimal->liste_arrete = malloc(nb * sizeof(int *));
    for(int i = 0; i<nb; i++)
    {
        minimal->liste_arrete[i] = malloc(3 * sizeof(int));
    }

    for(int i = 0; i<nb; i++)
    {
        alpha = (float)rand() / (float)RAND_MAX;

        if(recuperer_classe(graphe->liste_arrete[i][0], partition) != recuperer_classe(graphe->liste_arrete[i][1], partition) || alpha < p)
        {
            fusion(graphe->liste_arrete[i][0], graphe->liste_arrete[i][1], partition, hauteur);
            minimal->liste_arrete[ind][0] = graphe->liste_arrete[i][0];
            minimal->liste_arrete[ind][1] = graphe->liste_arrete[i][1];
            minimal->liste_arrete[ind][2] = graphe->liste_arrete[i][2];
            ind++;
        }
    }
    return minimal;
}