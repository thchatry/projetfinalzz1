#include "partition_arbo.h"

int ** matrice_adja(int n)
{
    int ** matrice = malloc(n*sizeof(int*));
    for(int i = 0; i<n; i++)
    {
        matrice[i] = malloc(n*sizeof(int));
    }


    srand(time(NULL));

    for(int i = 0; i<n; i++)
    {
        for(int j = 0; j<n; j++)
        {
            if(i == j)
            {
                matrice[i][j] = 0;
            }
            else
            {
                matrice[i][j] = rand()%2;
            }
        }
    }

    return matrice;
}

void liberer_matrice(int ** matrice, int n)
{
    for(int i = 0; i<n; i++)
    {
        free(matrice[i]);
    }
    free(matrice);
}

void crea_graphe_matrice(int ** matrice, int * partition, int * hauteur, int n)
{
    for(int i = 0; i<n; i++)
    {
        for(int j = i; j<n; j++)
        {
            if(matrice[i][j]) fusion(i, j, partition, hauteur);
        }
    }
}

void afficher_matrice(int ** matrice, int n, int p)
{
    for(int i = 0; i<n; i++)
    {
        for(int j = 0; j<p; j++)
        {
            printf("%d ", matrice[i][j]);
        }
        printf("\n");
    }
}