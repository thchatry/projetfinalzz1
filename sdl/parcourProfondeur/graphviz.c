#include "partition_arbo.h"

int crea_graphviz_partition(int * partition, int n, char * nom_png)
{
    int sys, nb;

    for(int i = 0; i<n; i++)
    {
        if(partition[i] != i) nb++;
    }

    char nom[n];
    char commande[100];

    Agraph_t * mon_graphe;
    Agnode_t ** noeud = malloc(n * sizeof(Agnode_t*));
    Agedge_t ** arrete = malloc(nb * sizeof(Agedge_t*));

    FILE * fichier;
    fichier = fopen("demo.dot", "w");
    if(fichier == NULL)
    {
        fprintf(stderr, "Impossible d'ouvrir le fichier");
        exit(EXIT_FAILURE);
    }

    GVC_t * graph_context;
    graph_context = gvContext();
    if(graph_context == NULL)
    {
        fprintf(stderr, "Impossible de créer le contexte de graphe");
        exit(EXIT_FAILURE);
    }

    mon_graphe = agopen("Mon Graphe", Agundirected, 0);

    for(int i = 0; i<n; i++)
    {
        sprintf(nom, "%d", i);
        noeud[i] = agnode(mon_graphe, nom, 1);
    }

    for(int i = 0; i<n; i++)
    {
        if(partition[i] != i) arrete[i] = agedge(mon_graphe, noeud[i], noeud[partition[i]], NULL, 1);
    }

    agwrite(mon_graphe, stdout);
    gvLayout(graph_context, mon_graphe, "dot");
    gvRender(graph_context, mon_graphe, "dot", fichier);

    sprintf(commande, "dot -Tpng demo.dot -o %s", nom_png);

    sys = system(commande);
    if (sys != 0)
    {
        fprintf(stderr, "Impossible de lancer la commande : dot -Tpng demo.dot -o graph.png");
    }

    free(noeud);
    free(arrete);
    gvFreeLayout(graph_context, mon_graphe);
    agclose(mon_graphe);
    fclose(fichier);
    return EXIT_SUCCESS;
}

int crea_graphviz_graphe(graphe_t * graphe, int nb, char * nom_png)
{
    int sys;

    char nom[nb];
    char commande[100];
    char valuation[nb];

    Agraph_t * mon_graphe;
    Agnode_t ** noeud = malloc(graphe->nb_noeud * sizeof(Agnode_t*));
    Agedge_t ** arrete = malloc(nb * sizeof(Agedge_t*));

    FILE * fichier;
    fichier = fopen("demo.dot", "w");
    if(fichier == NULL)
    {
        fprintf(stderr, "Impossible d'ouvrir le fichier");
        exit(EXIT_FAILURE);
    }

    GVC_t * graph_context;
    graph_context = gvContext();
    if(graph_context == NULL)
    {
        fprintf(stderr, "Impossible de créer le contexte de graphe");
        exit(EXIT_FAILURE);
    }

    mon_graphe = agopen("Mon Graphe", Agundirected, 0);

    for(int i = 0; i<graphe->nb_noeud; i++)
    {
        sprintf(nom, "%d", i);
        noeud[i] = agnode(mon_graphe, nom, 1);
    }

    for(int i = 0; i<nb; i++)
    {
        if(graphe->liste_arrete[i][0] != graphe->liste_arrete[i][1])
        {
            arrete[i] = agedge(mon_graphe, noeud[graphe->liste_arrete[i][0]], noeud[graphe->liste_arrete[i][1]], NULL, 1);
            sprintf(valuation, "%d", graphe->liste_arrete[i][2]);
            agsafeset(arrete[i], "label", valuation, "");
        }
    }
    if(!strcmp(nom_png, "labyrinthe.png")) agsafeset(mon_graphe, "nodesep", "1.5", "");

    agwrite(mon_graphe, stdout);
    gvLayout(graph_context, mon_graphe, "dot");
    gvRender(graph_context, mon_graphe, "dot", fichier);

    sprintf(commande, "dot -Tpng demo.dot -o %s", nom_png);

    sys = system(commande);
    if (sys != 0)
    {
        fprintf(stderr, "Impossible de lancer la commande : dot -Tpng demo.dot -o graph.png");
    }

    free(noeud);
    free(arrete);
    gvFreeLayout(graph_context, mon_graphe);
    agclose(mon_graphe);
    fclose(fichier);
    return EXIT_SUCCESS;
}