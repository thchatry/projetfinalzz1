#ifndef GAME_H
#define GAME_H

#include <stdio.h>
#include <math.h>


int carre(int x);
void recentre(int* x, int* y, int taille);
int collision(int x1, int x2, int y1, int y2, int taille1, int taille2);
void contact(int xj, int yj, int xr, int yr, int* vitesseX, int* vitesseY, int rebound, int vitesseJ1_X, int vitesseJ1_Y, int taillej, int tailler) ;



#endif