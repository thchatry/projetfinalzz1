#ifndef AFFICHAGE_H
#define AFFICHAGE_H

#include <stdio.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>






SDL_Texture * load_texture_from_image(char * file_image_name, SDL_Window * window, SDL_Renderer * renderer);
SDL_Texture * load_texture_from_font(char * file_font_name, int size_font, SDL_Color color, char * text_to_show, SDL_Window * window, SDL_Renderer * renderer);
void fond(SDL_Rect * dimension, SDL_Rect * destination);
void titre(SDL_Rect * dimension, SDL_Rect * source, SDL_Rect * destination);
void option(SDL_Rect * dimension, SDL_Rect * source, SDL_Rect * destination);
SDL_bool ecranTitre(SDL_Window * fenetre, SDL_Renderer * renderer);
void play_with_texture_1(SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer);
void (SDL_Renderer* renderer, int but,SDL_Texture* text_texture[11], int largeur);


#endif