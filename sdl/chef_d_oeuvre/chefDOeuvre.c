#include "chefDOeuvre.h"


int HAUTEUR = 700;
int LARGEUR = 1400;
int FPS = 30;
int TAILLE_J = 100;
int TAILLE_R = 100;


void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
            char const* msg,                                    // message à afficher
            SDL_Window* window,                                 // fenêtre à fermer
            SDL_Renderer* renderer) {                           // renderer à fermer
    char msg_formated[255];                                         
    int l;                                                          

    if (!ok) {                                                      
    strncpy(msg_formated, msg, 250);                                 
    l = strlen(msg_formated);                                        
    strcpy(msg_formated + l, " : %s\n");                     

    SDL_Log(msg_formated, SDL_GetError());                   
    }                                                               

    if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
    if (window != NULL)   SDL_DestroyWindow(window);                                        

    SDL_Quit();                                                     

    if (!ok) {                                                      
    exit(EXIT_FAILURE);                                              
  }                                                               
}

int main(int argc, char **argv) {



    (void)argc;
    (void)argv;

    SDL_Window* window = NULL;

    SDL_Renderer* rendererEcranTritre = NULL;
    SDL_Renderer* rendererGame = NULL;
    SDL_bool prog_on;

    window = SDL_CreateWindow("Ball-balo",
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, 
                            LARGEUR,
                            HAUTEUR,
                            SDL_WINDOW_OPENGL);

    if (window == NULL) {
        end_sdl(0, "ERROR WINDOW CREATION", window, rendererEcranTritre);
    }
    
    
    if (TTF_Init() < 0) end_sdl(0, "Couldn't initialize SDL TTF", window, rendererEcranTritre);

    rendererEcranTritre = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    prog_on = ecranTitre(window, rendererEcranTritre);
    if (rendererEcranTritre != NULL) SDL_DestroyRenderer(rendererEcranTritre);
    

    if(prog_on) {
        rendererGame = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
        
        
        SDL_Texture* my_texture; 
        SDL_Texture* bg_texture;
        SDL_Texture* ball_texture;



        my_texture = IMG_LoadTexture(rendererGame,"../img/ballRotation.png");
        if (my_texture == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", window, rendererGame);
        bg_texture = IMG_LoadTexture(rendererGame,"../img/background.png");
        if (bg_texture == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", window, rendererGame);
        ball_texture = IMG_LoadTexture(rendererGame,"../img/loic4.png");
        if (ball_texture == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", window, rendererGame);

        game(my_texture, bg_texture, ball_texture, window, rendererGame);


        SDL_DestroyTexture(my_texture);
        SDL_DestroyTexture(bg_texture);
        SDL_DestroyTexture(ball_texture);
        end_sdl(1, "Normal ending", window, rendererGame);
    }
    IMG_Quit();
}


void game(SDL_Texture* my_texture,
                            SDL_Texture* bg_texture,
                            SDL_Texture* ball_texture,
                            SDL_Window* window,
                            SDL_Renderer* renderer) {
    SDL_Rect 
        source = {0},                      // Rectangle définissant la zone de la texture à récupérer
        source2 = {0},
        window_dimensions = {0},           // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
        ball_rebound = {0},                 // Rectangle définissant où la zone_source doit être déposée dans le renderer
        ball_j1 = {0};                        // Rectangle définissant la zone de la ball_j1 joueur

    SDL_GetWindowSize(
        window,
        &window_dimensions.w,
        &window_dimensions.h);               // Récupération des dimensions de la fenêtre

    window_dimensions.w = LARGEUR;

    SDL_QueryTexture(my_texture, NULL, NULL,
                    &source.w, &source.h);  // Récupération des dimensions de l'image
    SDL_QueryTexture(ball_texture, NULL, NULL,
                    &source2.w, &source2.h);  // Récupération des dimensions de l'image


    int nb_images = 32;                         //  Il y a 8 vignette dans la ligne qui nous intéresse
    int offset_x = source.w / 8,                // La largeur d'une vignette de l'image
        offset_y = source.h / 4;                // La hauteur d'une vignette de l'image
    SDL_Rect state[32];   

    /* construction des différents rectangles autour de chacune des vignettes de la planche */
    int i = 0;                                   
    for (int y = 0; y < source.h ; y += offset_y) {
        for (int x = 0; x < source.w; x += offset_x) {
            state[i].x = x;
            state[i].y = y;
            state[i].w = offset_x;
            state[i].h = offset_y;
            ++i;
        }
    }


    // initialisation zone_source
    int origine[2] = {((window_dimensions.w - ball_rebound.w) /2), 100};
    ball_rebound.w = TAILLE_R;         // La ball_rebound est un zoom de la source
    ball_rebound.h = TAILLE_R;         // La ball_rebound est un zoom de la source
    ball_rebound.x = origine[0];
    ball_rebound.y = origine[1];

    // initialisation zone_ball
    ball_j1.w = TAILLE_J;
    ball_j1.h = TAILLE_J;
    ball_j1.x = 660;
    ball_j1.y = HAUTEUR - TAILLE_J;


    // initialisation Score
    int but=0;   // score de 0 au debut 

    char *score[11]={"0","1","2","3","4","5","6","7","8","9","vous avez gagne"};  // on fait 10 jongles pour gagner 

    TTF_Font* font = NULL;                                               // la variable 'police de caractère'
    font = TTF_OpenFont("../fonts/SIXTY.TTF", 100);                     // La police à charger, la taille désirée
    if (font == NULL) end_sdl(0, "Can't load font", window, renderer);

    TTF_SetFontStyle(font, TTF_STYLE_ITALIC | TTF_STYLE_BOLD);           // en italique, gras


    SDL_Color color = {255, 0, 0, 255};                                  // la couleur du texte
    SDL_Surface* text_surface[11];
    SDL_Texture* text_texture[11];                                    // la texture qui contient le texte

    for(int j = 0; j <= 10; j++) {
        text_surface[j] = TTF_RenderText_Blended(font, score[j], color);      // création du texte dans la surface 
        if (text_surface[j] == NULL) end_sdl(0, "Can't create text surface", window, renderer);

        text_texture[j] = SDL_CreateTextureFromSurface(renderer, text_surface[j]); // transfert de la surface à la texture
        if (text_texture[j] == NULL) end_sdl(0, "Can't create texture from surface", window, renderer);
        SDL_FreeSurface(text_surface[j]);
    }


    SDL_Event event; 
    i = 0;
    int collisionPasse = 0;
    int vitesseX = 0;           // vitesse initiale de de la balle rebondissante sur x
    int vitesseY = 0;           // vitesse initiale de de la balle rebondissante sur y  
    int vitesseJ1_X = 0;        // vitesse initiale du joueur sur x
    int vitesseJ1_Y = 0;        // vitesse initiale du joueur sur y 
    int accelJ1_Y = 25;         // acceleration prise par la balle a l'impulsion du saut
    int vitesseDeplacement = 15;
    float graviteball = 1.5;        // graviteball effective
    float graviteJoueur = 3;
    float rebound = 1.3;          //[1; 1,1], 1.06 stabilite, 

    SDL_bool program_on = SDL_TRUE;
    while (program_on) {

        // Boucle des evenements
        while(program_on && SDL_PollEvent(&event)){      

            switch(event.type){                       
            case SDL_QUIT :                          
                program_on = SDL_FALSE;                 
                break;

            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {             // la touche appuyée est ...
                case SDLK_d:
                    vitesseJ1_X = vitesseDeplacement;
                    break;
                case SDLK_q:
                    vitesseJ1_X = -vitesseDeplacement;
                    break;
                case SDLK_s:
                    vitesseJ1_X = 0;
                    break;
                case SDLK_z:
                    vitesseJ1_Y = -accelJ1_Y;
                    break;
                case SDLK_p:                                // 'q'
                    program_on = 0;                         // 'escape' ou 'q', d'autres façons de quitter le programme                                     
                    break;
                case SDLK_h:
                    ball_rebound.y=100;
                    ball_rebound.x=600;
                    vitesseX=0;
                    vitesseY=0;
                    but = 0;
                    break;
                default:                                    // Une touche appuyée qu'on ne traite pas
                    break;
                }
            default:                                  
                break;
            }
        }

        // recalcule des coordonées
        ball_rebound.x += vitesseX;
        ball_rebound.y += vitesseY;

        ball_j1.x += vitesseJ1_X;
        ball_j1.y += vitesseJ1_Y;

        if(ball_rebound.x >= (LARGEUR - TAILLE_R) || ball_rebound.x <= 0) {
            vitesseX = -vitesseX;
            if(ball_rebound.x <= 0) {
                ball_rebound.x = 0;
            } else {
                ball_rebound.x = (LARGEUR - TAILLE_R);
            }
        }
        if(ball_rebound.y >= 600) {
            ball_rebound.y = 600;
            vitesseY = -vitesseY * rebound;
            but=0; // score à 0 quand on touche le sol
        }

        if(ball_j1.x >= (LARGEUR - TAILLE_R) || ball_j1.x <= 0) {
            vitesseJ1_X = -vitesseJ1_X;
        }
        if(ball_j1.y >= (HAUTEUR - TAILLE_J)) {
            ball_j1.y = (HAUTEUR - TAILLE_J);
            vitesseJ1_Y = 0;
        }

        vitesseY += graviteball;
        vitesseJ1_Y += graviteJoueur;


        //gestion colision
        if(collision(ball_rebound.x, ball_j1.x, ball_rebound.y, ball_j1.y, TAILLE_R, TAILLE_J)) {
            if(!collisionPasse) {
                contact(ball_j1.x, ball_j1.y, ball_rebound.x, ball_rebound.y, &vitesseX, &vitesseY, rebound, vitesseJ1_X, vitesseJ1_Y, TAILLE_J, TAILLE_R);
                collisionPasse = 1;
                but++;
            }
        } else {
            collisionPasse = 0;
        }

        //affichage du backgroud
        play_with_texture_1(bg_texture, window, renderer);

        afficheScore(renderer,but,text_texture, LARGEUR);

        // Préparation de l'affichage source
        SDL_RenderCopy(renderer,                
                        my_texture, 
                        &state[i], 
                        &ball_rebound);
        i = (i + 1) % nb_images;                  // Passage à l'image suivante, le modulo car l'animation est cyclique 

        // Préparation de l'affichage ball_j1
        SDL_RenderCopy(renderer,                  
                    ball_texture, 
                    &source2,
                    &ball_j1
        );

        SDL_RenderPresent(renderer);             
        SDL_Delay(1000/FPS);
        SDL_RenderClear(renderer);                        
    }       

    for(int j = 0; j < 11; j++) {
        SDL_DestroyTexture(text_texture[j]);
    }
}