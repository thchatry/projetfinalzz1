#include "affichage.h"



SDL_Texture * load_texture_from_image(char * file_image_name, SDL_Window * window, SDL_Renderer * renderer)
{
    SDL_Surface *my_image = NULL;
    SDL_Texture* my_texture = NULL;

    my_image = IMG_Load(file_image_name);

    if (my_image == NULL) end_sdl(0, "Chargement de l'image impossible", window, renderer);

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
    SDL_FreeSurface(my_image);
    if (my_texture == NULL) end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

    return my_texture;
}

SDL_Texture * load_texture_from_font(char * file_font_name, int size_font, SDL_Color color, char * text_to_show, SDL_Window * window, SDL_Renderer * renderer)
{
  SDL_Texture * texture = NULL;
  TTF_Font * font = NULL;
  SDL_Surface * surface = NULL;

  font = TTF_OpenFont(file_font_name, size_font);
  if (font == NULL) end_sdl(0, "Can't load font", window, renderer);

  surface = TTF_RenderText_Blended(font, text_to_show, color);
  if (surface == NULL) end_sdl(0, "Can't create text surface", window, renderer);

  texture = SDL_CreateTextureFromSurface(renderer, surface);
  if (texture == NULL) end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

  TTF_CloseFont(font);
  SDL_FreeSurface(surface);

  return texture;
}

void fond(SDL_Rect * dimension, SDL_Rect * destination)
{
  destination->w = dimension->w;
  destination->h = dimension->h;
}

void titre(SDL_Rect * dimension, SDL_Rect * source, SDL_Rect * destination)
{
  destination->x = dimension->w/4 - source->w/2;
  destination->y = dimension->h/5 - source->h/2;
  destination->w = source->w;
  destination->h = source->h;
}

void option(SDL_Rect * dimension, SDL_Rect * source, SDL_Rect * destination)
{
  destination->x = dimension->w/4 - source->w/2;
  destination->y = dimension->h/3 - source->h/2;
  destination->w = source->w;
  destination->h = source->h;
}

SDL_bool ecranTitre(SDL_Window * fenetre, SDL_Renderer * renderer)
{   
    SDL_bool prog_on = SDL_TRUE, jouer = SDL_FALSE;
    SDL_Event event;
    SDL_Rect
            dimension = {0},
            source_fond = {0}, source_titre = {0}, source_jouer = {0},
            destination_fond = {0}, destination_titre = {0}, destination_jouer = {0};
    SDL_Color color = {255, 255, 255, 255};

    SDL_Texture * texture_fond = load_texture_from_image("../img/fond.jpg", fenetre, renderer);
    SDL_Texture * texture_titre = load_texture_from_font("../fonts/bebas.ttf", 150, color, "Balli Ballo", fenetre, renderer);
    SDL_Texture * texture_jouer = load_texture_from_font("../fonts/bebas.ttf", 100, color, "Jouer", fenetre, renderer);

    int souris_x, souris_y;

    SDL_GetWindowSize(fenetre, &dimension.w, &dimension.h);
    SDL_QueryTexture(texture_fond, NULL, NULL, &source_fond.w, &source_fond.h);
    SDL_QueryTexture(texture_titre, NULL, NULL, &source_titre.w, &source_titre.h);
    SDL_QueryTexture(texture_jouer, NULL, NULL, &source_jouer.w, &source_jouer.h);
    fond(&dimension, &destination_fond);
    titre(&dimension, &source_titre, &destination_titre);
    option(&dimension, &source_jouer, &destination_jouer);

    SDL_RenderCopy(renderer, texture_fond, &source_fond, &destination_fond);
    SDL_RenderCopy(renderer, texture_titre, &source_titre, &destination_titre);
    SDL_RenderCopy(renderer, texture_jouer, &source_jouer, &destination_jouer);
    SDL_RenderPresent(renderer);

    while(prog_on && !jouer)
    {
        //Boucle qui récupère les derniers inputs
        while(prog_on && SDL_PollEvent(&event))
        {
            switch(event.type)
            {
            case SDL_QUIT:
                prog_on = SDL_FALSE;
                break;

            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                case SDLK_b:
                    jouer = SDL_FALSE;
                    break;
                
                case SDLK_p:
                    jouer = SDL_TRUE;
                    break;

                default:
                    break;
                }
                break;
            
            case SDL_MOUSEBUTTONDOWN:
                if(SDL_GetMouseState(&souris_x, &souris_y) && SDL_BUTTON(SDL_BUTTON_LEFT))
                {
                if( (souris_x > destination_jouer.x && souris_x < destination_jouer.x + destination_jouer.w) && (souris_y > destination_jouer.y && souris_y < destination_jouer.y + destination_jouer.h) )
                {
                    jouer = SDL_TRUE;
                }
                }
                break;

            default:
                break;
            }
        }
        SDL_Delay(50);
    }
    SDL_RenderClear(renderer);
    return prog_on;
}


void play_with_texture_1(SDL_Texture *my_texture, SDL_Window *window,
                         SDL_Renderer *renderer) {
  SDL_Rect 
          source = {0},                         // Rectangle définissant la zone de la texture à récupérer
          window_dimensions = {0},              // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
          ball_rebound = {0};                    // Rectangle définissant où la zone_source doit être déposée dans le renderer

  SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);                    // Récupération des dimensions de la fenêtre
  SDL_QueryTexture(my_texture, NULL, NULL,
                   &source.w, &source.h);       // Récupération des dimensions de l'image

  ball_rebound = window_dimensions;              // On fixe les dimensions de l'affichage à  celles de la fenêtre

  /* On veut afficher la texture de façon à ce que l'image occupe la totalité de la fenêtre */

  SDL_RenderCopy(renderer, my_texture,
                 &source,
                 &ball_rebound);                 // Création de l'élément à afficher
}



void afficheScore(SDL_Renderer* renderer, int but, SDL_Texture* text_texture[11], int largeur){

    if(but > 11) {
        but=10;
    }

    SDL_Rect pos = {0,0,0,0};                                         // rectangle où le texte va être prositionné
    SDL_QueryTexture(text_texture[but], NULL, NULL, &pos.w, &pos.h);          // récupération de la taille (w, h) du texte 
    pos.x = largeur/2 -pos.w/2;                                          //milieu haut de l'ecran
    SDL_RenderCopy(renderer, text_texture[but], NULL, &pos);                  // Ecriture du texte dans le renderer   
} 