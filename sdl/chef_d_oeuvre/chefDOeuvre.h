#ifndef CHEFDOEUVRE_H
#define CHEFDOEUVRE_H


#include "game.h"
#include "affichage.h"


#include <stdio.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>


void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
            char const* msg,                                    // message à afficher
            SDL_Window* window,                                 // fenêtre à fermer
            SDL_Renderer* renderer) ;
void game(SDL_Texture* my_texture, SDL_Texture* bg_texture, SDL_Texture* ball_texture, SDL_Window* window, SDL_Renderer* renderer);

#endif