#include "game.h"

int carre(int x) {
    return x*x;
}

void recentre(int* x, int* y, int taille) {
    *x = *x + (taille/2);
    *y = *y + (taille/2);
}

int collision(int x1, int x2, int y1, int y2, int taille1, int taille2) {
    recentre(&x1, &y1,taille1);
    recentre(&x2, &y2, taille2);
    float distance;
    distance = sqrt(carre(x1-x2) + carre(y1-y2));
    return (distance <= ((taille1+taille2)/2));
}

void contact(int xj, int yj, int xr, int yr, int* vitesseX, int* vitesseY, int rebound, int vitesseJ1_X, int vitesseJ1_Y, int taillej, int tailler) {
    recentre(&xj, &yj, taillej);
    recentre(&xr, &yr, tailler);
    int dx = xr - xj;
    int dy = yr - yj;

    double phi = atan((float) ((double)dy/((double)dx)));               //angle de colision/ angle de renvoie
    double vitesse = sqrt(carre(*vitesseX) + carre(*vitesseY));         //vitesse "absolu" 

    *vitesseX = ((int) vitesse * cos(phi)) + vitesseJ1_X;
    *vitesseY = ((int) (vitesse * cos(M_PI/2 - phi))) + vitesseJ1_Y;
    *vitesseY = *vitesseY * rebound;

    if(xj > xr) {
        *vitesseY = -*vitesseY;
        *vitesseX = -*vitesseX;
    }


}


